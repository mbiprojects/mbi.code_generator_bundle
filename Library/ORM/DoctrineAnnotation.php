<?php

namespace Mbi\CodeGeneratorBundle\Library\ORM;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplate;

/**
 * DoctrineAnnotation
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class DoctrineAnnotation extends AnnotationTemplate
{

    /**
     * @var string
     */
    protected $class;

    /**
     * @var array|null
     */
    protected $options;

    /**
     * @var string
     */
    protected $ident = 'doctrine';

    /**
     * getClass
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * setClass
     *
     * @param string $class
     *
     * @return DoctrineAnnotation
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * getOptions
     *
     * @return array|null
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * setOptions
     *
     * @param array|null $options
     *
     * @return DoctrineAnnotation
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * render
     *
     * @return string
     */
    public function render()
    {
        return '@'.$this->getClass().'('.$this->prepareOptionsForIdent().')';
    }

    /**
     * prepareOptionsForIdent
     *
     * @return string
     */
    public function prepareOptionsForIdent()
    {
        if (is_null($this->options)) {
            return '';
        }
        $options = [];
        foreach ($this->options as $key => $value) {
            if (is_string($value)) {
                $options[] = $key.'="'.$value.'"';
            } else {
                $options[] = $key.'='.$value;
            }
        }

        return implode(', ', $options);
    }
}
