<?php

namespace Mbi\CodeGeneratorBundle\Library\ORM;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * RepositoryFunctionsTrait
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
trait RepositoryFunctionsTrait
{

    /**
     * @param mixed $entity
     * @param bool  $flush
     */
    public function persist($entity, $flush = true)
    {
        /** @var EntityManager $em */
        /** @var EntityRepository $this */
        $em = $this->getEntityManager();
        $em->persist($entity);

        if ($flush) {
            $em->flush();
        }
    }

    /**
     * @param mixed $entity
     */
    public function detach($entity)
    {
        /** @var EntityManager $em */
        /** @var EntityRepository $this */
        $em = $this->getEntityManager();
        $em->detach($entity);
    }

    /**
     * @param mixed $entity
     * @param bool  $flush
     */
    public function remove($entity, $flush = true)
    {
        /** @var EntityManager $em */
        /** @var EntityRepository $this */
        $em = $this->getEntityManager();
        $em->remove($entity);

        if ($flush) {
            $em->flush();
        }
    }

    /**
     * @param mixed $entity
     */
    public function flush($entity = null)
    {
        /** @var EntityManager $em */
        /** @var EntityRepository $this */
        $em = $this->getEntityManager();
        $em->flush($entity);
    }
}
