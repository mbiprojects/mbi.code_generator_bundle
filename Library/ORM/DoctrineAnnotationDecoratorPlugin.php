<?php

namespace Mbi\CodeGeneratorBundle\Library\ORM;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationDecoratorPluginInterface;
use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\DataType\StringType;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;

/**
 * DoctrineAnnotationDecoratorPlugin
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class DoctrineAnnotationDecoratorPlugin implements AnnotationDecoratorPluginInterface
{

    /**
     * @var DoctrineAnnotationFactory
     */
    private $annotationFactory;

    /**
     * __construct
     *
     * @param DoctrineAnnotationFactory $annotationFactory
     */
    public function __construct(DoctrineAnnotationFactory $annotationFactory)
    {
        $this->annotationFactory = $annotationFactory;
    }

    /**
     * @inheritDoc
     */
    public function getIdent()
    {
        return __CLASS__;
    }

    /**
     * @inheritDoc
     */
    public function decorate(ClassTemplate $classTemplate)
    {
        //decorate column-annotations
        foreach ($classTemplate->getProperties() as $property) {
            if (!$property->isDoctrineProperty()) {
                continue;
            }

            //TODO: refactor options resolving
            $columnAnnotation = $this->annotationFactory->createColumnAnnotationByDataType(
                $property->getDataType(),
                $this->getSupportedOptionsFromProperty($property)
            );

            $property->getAnnotationSet()->addTemplate($columnAnnotation);
        }
    }

    /**
     * getSupportedOptionsFromProperty
     *
     * @param PropertyTemplate $propertyTemplate
     *
     * @return array
     */
    protected function getSupportedOptionsFromProperty(PropertyTemplate $propertyTemplate)
    {
        $return = [];
        if ($propertyTemplate->getDataType() instanceof StringType
            && $propertyTemplate->hasOption('length')) {
            $return['length'] = $propertyTemplate->getOption('length');
        }

        if ($propertyTemplate->isNullable()) {
            $return['nullable'] = 'true';
        }

        return $return;
    }
}
