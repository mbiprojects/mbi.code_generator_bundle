<?php

namespace Mbi\CodeGeneratorBundle\Library\ORM;

use Doctrine\ORM\Mapping as ORM;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplateFactory;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplateFactoryInterface;
use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\DataType\ObjectType;

/**
 * DoctrineAnnotationFactory
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class DoctrineAnnotationFactory extends AnnotationTemplateFactory implements AnnotationTemplateFactoryInterface
{

    /**
     * createByInstuction
     *
     * @param AnnotationInstruction $instruction
     *
     * @return DoctrineAnnotation|void
     */
    public function createByInstuction(AnnotationInstruction $instruction)
    {
        $annotation = $this->createInstance();
        $annotation->setClass($instruction->getClass());
        $annotation->setOptions($instruction->getOptions());

        return $annotation;
    }

    /**
     * supportInstruction
     *
     * @param AnnotationInstruction $instruction
     *
     * @return bool
     */
    public function supportInstruction(AnnotationInstruction $instruction)
    {
        return ($instruction instanceof DoctrineAnnotationInstruction);
    }

    /**
     * createAnnotation
     *
     * @param string     $class
     * @param array|null $options
     *
     * @return DoctrineAnnotation
     */
    public function createAnnotation(string $class, ?array $options)
    {
        $annotation = $this->createInstance();
        $annotation->setClass($class);
        $annotation->setOptions($options);

        return $annotation;
    }

    /**
     * createColumnAnnotationByDataType
     *
     * @param DataType $dataType
     * @param array    $options
     *
     * @return DoctrineAnnotation
     * @throws \Exception
     */
    public function createColumnAnnotationByDataType(DataType $dataType, array $options = null)
    {
        return $this->createAnnotation(ORM\Column::class, $this->assembleColumnOptions($dataType, $options));
    }

    /**
     * createInstance
     *
     * @return DoctrineAnnotation
     */
    protected function createInstance()
    {
        return new DoctrineAnnotation();
    }

    /**
     * assembleOptions
     *
     * @param DataType $dataType
     * @param array    $additionalOptions
     *
     * @return array
     * @throws \Exception
     */
    protected function assembleColumnOptions(DataType $dataType, ?array $additionalOptions)
    {
        $type = null;
        switch ($dataType->getTypeIdent()) {
            case DataType::STRING:
                $type = 'text';
                break;
            case DataType::BOOLEAN:
                $type = 'boolean';
                break;
            case DataType::FLOAT:
                $type = 'float';
                break;
            case DataType::INTEGER:
                $type = 'integer';
                break;
            case DataType::OBJECT:
                if ($dataType instanceof ObjectType && $dataType->isClass(\DateTime::class)) {
                    $type = 'datetime';
                }
                break;
        }

        if (is_null($type)) {
            throw new \Exception('DataType not supported for doctrine annotation (yet): '.$dataType->getTypeIdent());
        }

        $options = ['type' => $type];
        if (is_array($additionalOptions)) {
            $options = array_merge($options, $additionalOptions);
        }

        return $options;
    }
}
