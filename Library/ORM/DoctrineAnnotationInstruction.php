<?php

namespace Mbi\CodeGeneratorBundle\Library\ORM;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationInstruction;

/**
 * DoctrineAnnotationInstruction
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class DoctrineAnnotationInstruction extends AnnotationInstruction
{

    /**
     * @var string
     */
    protected $class;

    /**
     * @var array|null
     */
    protected $options;

    /**
     * createByClass
     *
     * @param string     $class
     * @param array|null $options
     *
     * @return DoctrineAnnotationInstruction
     */
    public static function createByClass(string $class, ?array $options = null)
    {
        $annotation = new self();
        $annotation->setClass($class);
        $annotation->setOptions($options);

        return $annotation;
    }

    /**
     * getClass
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * setClass
     *
     * @param string $class
     *
     * @return DoctrineAnnotationInstruction
     */
    public function setClass(string $class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * getOptions
     *
     * @return array|null
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * setOptions
     *
     * @param array|null $options
     *
     * @return DoctrineAnnotationInstruction
     */
    public function setOptions(?array $options)
    {
        $this->options = $options;

        return $this;
    }
}
