<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

/**
 * DataTypeOption
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class DataTypeOption
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $validationRegex;

    /**
     * @var string
     */
    protected $value;

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setName
     *
     * @param string $name
     *
     * @return DataTypeOption
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getValidationRegex
     *
     * @return string
     */
    public function getValidationRegex()
    {
        return $this->validationRegex;
    }

    /**
     * setValidationRegex
     *
     * @param string $validationRegex
     *
     * @return DataTypeOption
     */
    public function setValidationRegex($validationRegex)
    {
        $this->validationRegex = $validationRegex;

        return $this;
    }

    /**
     * getValue
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * setValue
     *
     * @param string $value
     *
     * @return DataTypeOption
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
