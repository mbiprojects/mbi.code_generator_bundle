<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

use Mbi\CodeGeneratorBundle\Library\ClassProxyFactory;

/**
 * DataTypeFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class DataTypeFactory
{

    /**
     * @var ClassProxyFactory
     */
    private $classProxyFactory;

    /**
     * __construct
     *
     * @param ClassProxyFactory $classProxyFactory
     */
    public function __construct(ClassProxyFactory $classProxyFactory)
    {
        $this->classProxyFactory = $classProxyFactory;
    }

    /**
     * createByIdent
     *
     * @param string $ident
     *
     * @return DataType
     * @throws \Exception
     */
    public function createByIdent(string $ident)
    {
        $dataTypeOptions = [$this->createOption('nullable', '/^(0|1)$/')];
        switch ($ident) {
            case DataType::STRING:
                $dataType          = new StringType();
                $dataTypeOptions[] = $this->createOption('length', '/^[0-9]+$/');
                break;
            case DataType::INTEGER:
                $dataType = new IntegerType();
                break;
            case DataType::FLOAT:
                $dataType = new FloatType();
                break;
            case DataType::BOOLEAN:
                $dataType = new BooleanType();
                break;
            case DataType::DATETIME:
                $dataType = $this->createClassType(\DateTime::class);
                break;
            default:
                if (class_exists($ident)) {
                    $dataType = $this->createClassType($ident);
                } else {
                    throw new \Exception('Invalid ident for datatype factory given: '.$ident);
                }
                break;
        }

        $dataType->setOptions($dataTypeOptions);

        return $dataType;
    }

    /**
     * createClassType
     *
     * @param string $class
     *
     * @return ObjectType
     * @throws \Exception
     */
    public function createClassType(string $class)
    {
        if (!class_exists($class)) {
            throw new \Exception('invalid class given for ClassType: '.$class);
        }

        return new ObjectType($this->classProxyFactory->createClassProxy($class));
    }

    /**
     * createOption
     *
     * @param string $name
     * @param string $validationRegex
     *
     * @return DataTypeOption
     */
    private function createOption(string $name, string $validationRegex)
    {
        return (new DataTypeOption())->setName($name)->setValidationRegex($validationRegex);
    }
}
