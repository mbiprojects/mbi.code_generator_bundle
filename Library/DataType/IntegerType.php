<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

/**
 * IntegerType
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class IntegerType extends DataType
{

    /**
     * @var string
     */
    protected $typeIdent = self::INTEGER;
}
