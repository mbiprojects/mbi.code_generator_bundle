<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

/**
 * StringType
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class StringType extends DataType
{

    /**
     * @var string
     */
    protected $typeIdent = self::STRING;
}
