<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

/**
 * FloatType
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class FloatType extends DataType
{

    /**
     * @var string
     */
    protected $typeIdent = self::FLOAT;
}
