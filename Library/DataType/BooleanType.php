<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

/**
 * BooleanType
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class BooleanType extends DataType
{

    /**
     * @var string
     */
    protected $typeIdent = self::BOOLEAN;
}
