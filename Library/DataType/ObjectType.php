<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

use Mbi\CodeGeneratorBundle\Library\ClassProxy;

/**
 * ObjectType
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ObjectType extends DataType
{

    /**
     * @var string
     */
    protected $typeIdent = self::OBJECT;

    /**
     * @var ClassProxy
     */
    protected $classProxy;

    /**
     * __construct
     *
     * @param ClassProxy $classProxy
     */
    public function __construct(ClassProxy $classProxy)
    {
        $this->classProxy = $classProxy;
    }

    /**
     * getClassBaseName
     *
     * @return string
     */
    public function getClassBaseName()
    {
        return $this->classProxy->getClassBaseName();
    }

    /**
     * getFullQualifiedClassName
     *
     * @return string
     */
    public function getFullQualifiedClassName()
    {
        return $this->classProxy->getFullQualifiedClassName();
    }

    /**
     * getClassNameSpace
     *
     * @return string
     */
    public function getClassNameSpace()
    {
        return $this->classProxy->getNamespace();
    }

    /**
     * getTypeHint
     *
     * @return string
     */
    public function getTypeHint()
    {
        return $this->getClassBaseName();
    }

    /**
     * isClass
     *
     * @param string $class
     *
     * @return boolean
     */
    public function isClass(string $class)
    {
        return $this->getFullQualifiedClassName() == $class;
    }
}
