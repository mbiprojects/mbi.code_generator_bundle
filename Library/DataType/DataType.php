<?php

namespace Mbi\CodeGeneratorBundle\Library\DataType;

/**
 * DataType
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
abstract class DataType
{

    const STRING = 'string';
    const BOOLEAN = 'bool';
    const INTEGER = 'int';
    const FLOAT = 'float';
    const OBJECT = 'object';
    const DATETIME = 'datetime';

    const ALLOWED_SCALAR_TYPES = [
        self::STRING,
        self::BOOLEAN,
        self::INTEGER,
        self::DATETIME,
        self::FLOAT,
    ];

    /**
     * @var string
     */
    protected $typeIdent;

    /**
     * @var DataTypeOption[]
     */
    protected $options = [];

    /**
     * getTypeIdent
     *
     * @return string
     */
    public function getTypeIdent()
    {
        return $this->typeIdent;
    }

    /**
     * @param string $typeIdent
     *
     * @return DataType
     */
    public function setTypeIdent(string $typeIdent): DataType
    {
        $this->typeIdent = $typeIdent;

        return $this;
    }

    /**
     * getTypeHint
     *
     * @return string
     */
    public function getTypeHint()
    {
        return $this->getTypeIdent();
    }

    /**
     * getOptions
     *
     * @return DataTypeOption[]|array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * setOptions
     *
     * @param DataTypeOption[] $options
     *
     * @return DataType
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }
}
