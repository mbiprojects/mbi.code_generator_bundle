<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * TemplateStack
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassTemplateStack
{

    /**
     * @var ClassTemplate[]
     */
    protected $classTemplates = [];

    /**
     * create
     *
     * @return ClassTemplateStack
     */
    public static function create()
    {
        return new self();
    }

    /**
     * add
     *
     * @param ClassTemplate $classTemplate
     *
     * @return ClassTemplateStack
     */
    public function add(ClassTemplate $classTemplate)
    {
        $this->classTemplates[$classTemplate->getFullClass()] = $classTemplate;

        return $this;
    }

    /**
     * getGeneratedTemplates
     *
     * @return ClassTemplate[]|array
     */
    public function getGeneratedTemplates()
    {
        return $this->classTemplates;
    }

    /**
     * getCount
     *
     * @return int
     */
    public function getCount()
    {
        return count($this->classTemplates);
    }

    /**
     * getTemplateByTag
     *
     * @param string $tag
     *
     * @return ClassTemplate|null
     */
    public function getTemplateByTag(string $tag)
    {
        foreach ($this->classTemplates as $classTemplate) {
            if ($classTemplate->getTemplateTag() == $tag) {
                return $classTemplate;
            }
        }

        return null;
    }

    /**
     * getGeneratedClasses
     *
     * @return array
     */
    public function getGeneratedClasses()
    {
        $classes = [];
        foreach ($this->classTemplates as $classTemplate) {
            $classes[] = $classTemplate->getFullClass();
        }

        return $classes;
    }
}
