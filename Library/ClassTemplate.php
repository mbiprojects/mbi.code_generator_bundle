<?php

namespace Mbi\CodeGeneratorBundle\Library;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationSetAwareInterface;
use Mbi\CodeGeneratorBundle\Library\DataType\ObjectType;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Property\InvalidPropertyException;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationSetAwareTrait;

/**
 * ClassTemplate
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassTemplate implements AnnotationSetAwareInterface
{

    use AnnotationSetAwareTrait;

    /**
     * @var ClassProxy
     */
    protected $classProxy;

    /**
     * @var PropertyTemplate[]
     */
    protected $properties = [];

    /**
     * @var MethodTemplate[]
     */
    protected $methods = [];

    /**
     * @var ClassProxy[]
     */
    protected $traits = [];

    /**
     * @var ClassProxy[]
     */
    protected $interfaces = [];

    /**
     * @var ClassProxy
     */
    protected $parentClassProxy;

    /**
     * @var string
     */
    protected $templateTag;

    /**
     * @var array
     */
    protected $usedClass = [];

    /**
     * @var string
     */
    protected $classTemplatePath;

    /**
     * @var string
     */
    protected $classTemplateFile;

    /**
     * getClassProxy
     *
     * @return ClassProxy
     */
    public function getClassProxy()
    {
        return $this->classProxy;
    }

    /**
     * @param ClassProxy $classProxy
     *
     * @return ClassTemplate
     */
    public function setClassProxy(ClassProxy $classProxy): ClassTemplate
    {
        $this->classProxy = $classProxy;

        return $this;
    }

    /**
     * getClassBaseName
     *
     * @return string
     */
    public function getClassBaseName()
    {
        return $this->classProxy->getClassBaseName();
    }

    /**
     * getClassVarName
     *
     * @return string
     */
    public function getClassVarName()
    {
        return lcfirst($this->getClassBaseName());
    }

    /**
     * getNamespace
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->getClassProxy()->getNamespace();
    }

    /**
     * getFullClass
     *
     * @return string
     */
    public function getFullClass()
    {
        return $this->getNamespace().'\\'.$this->getClassBaseName();
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getClassProxy()->getPath();
    }

    /**
     * getProperties
     *
     * @return PropertyTemplate[]|array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param mixed $properties
     *
     * @return ClassTemplate
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * addProperty
     *
     * @param PropertyTemplate $propertyTemplate
     *
     * @return ClassTemplate
     */
    public function addProperty(PropertyTemplate $propertyTemplate)
    {
        $this->properties[$propertyTemplate->getName()] = $propertyTemplate;

        return $this;
    }

    /**
     * getProperty
     *
     * @param string $name
     *
     * @return PropertyTemplate
     * @throws InvalidPropertyException
     */
    public function getProperty(string $name)
    {
        if (!array_key_exists($name, $this->properties)) {
            throw new InvalidPropertyException('property of name '.$name.' does not exist!');
        }

        return $this->properties[$name];
    }

    /**
     * getParentClassProxy
     *
     * @return ClassProxy
     */
    public function getParentClassProxy()
    {
        return $this->parentClassProxy;
    }

    /**
     * @param ClassProxy $parentClassProxy
     *
     * @return ClassTemplate
     */
    public function setParentClassProxy(?ClassProxy $parentClassProxy): ClassTemplate
    {
        $this->parentClassProxy = $parentClassProxy;

        return $this;
    }

    /**
     * addMethod
     *
     * @param MethodTemplate $methodTemplate
     *
     * @return ClassTemplate
     */
    public function addMethod(MethodTemplate $methodTemplate)
    {
        $this->methods[$methodTemplate->getName()] = $methodTemplate;

        return $this;
    }

    /**
     * addTrait
     *
     * @param ClassProxy $trait
     *
     * @return ClassTemplate
     */
    public function addTrait(ClassProxy $trait)
    {
        //TODO: check if trait exists

        $this->traits[] = $trait;

        return $this;
    }

    /**
     * addInterface
     *
     * @param ClassProxy $interface
     *
     * @return ClassTemplate
     */
    public function addInterface(ClassProxy $interface)
    {
        //TODO: check if interface exists

        $this->interfaces[] = $interface;

        return $this;
    }

    /**
     * getTraits
     *
     * @return ClassProxy[]|array
     */
    public function getTraits()
    {
        return $this->traits;
    }

    /**
     * getInterfaces
     *
     * @return ClassProxy[]|array
     */
    public function getInterfaces()
    {
        return $this->interfaces;
    }

    /**
     * getMethods
     *
     * @return MethodTemplate[]|array
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * getUsedNamespaces
     *
     * @return array
     */
    public function getUsedClasses()
    {
        $used = $this->usedClass;
        if (count($this->traits)) {
            foreach ($this->traits as $traitProxy) {
                if (!in_array($traitProxy->getFullQualifiedClassName(), $used)) {
                    $used[] = $traitProxy->getFullQualifiedClassName();
                }
            }
        }

        if (count($this->interfaces)) {
            foreach ($this->interfaces as $tinterfaceProxy) {
                if (!in_array($tinterfaceProxy->getFullQualifiedClassName(), $used)) {
                    $used[] = $tinterfaceProxy->getFullQualifiedClassName();
                }
            }
        }

        if ($this->parentClassProxy) {
            if (!in_array($this->parentClassProxy->getFullQualifiedClassName(), $used)) {
                $used[] = $this->parentClassProxy->getFullQualifiedClassName();
            }
        }

        if (count($this->methods)) {
            foreach ($this->methods as $method) {
                foreach ($method->getArguments() as $argument) {
                    $dataType = $argument->getDataType();
                    if ($dataType instanceof ObjectType) {
                        if (!in_array($dataType->getFullQualifiedClassName(), $used)) {
                            $used[] = $dataType->getFullQualifiedClassName();
                        }
                    }
                }
            }
        }

        return $used;
    }

    /**
     * addUsedClass
     *
     * @param string $class
     *
     * @return ClassTemplate
     */
    public function addUsedClass(string $class)
    {
        $this->usedClass[] = $class;

        return $this;
    }

    /**
     * getTemplateTag
     *
     * @return string
     */
    public function getTemplateTag()
    {
        return $this->templateTag;
    }

    /**
     * @param string $templateTag
     *
     * @return ClassTemplate
     */
    public function setTemplateTag(string $templateTag): ClassTemplate
    {
        $this->templateTag = $templateTag;

        return $this;
    }

    /**
     * getInterfaceList
     *
     * @return string
     */
    public function getInterfaceList()
    {
        $interfaceNames = [];
        foreach ($this->getInterfaces() as $interfaceClassProxies) {
            $interfaceNames[] = $interfaceClassProxies->getClassBaseName();
        }

        return implode(', ', $interfaceNames);
    }

    /**
     * setClassTemplateFile
     *
     * @param string $classTemplatePath
     * @param string $classTemplateFile
     *
     * @return void
     */
    public function setClassTemplateFile(string $classTemplateFile, ?string $classTemplatePath = null)
    {
        $this->classTemplateFile = $classTemplateFile;
        $this->classTemplatePath = $classTemplatePath;
    }

    /**
     * getClassTemplatePath
     *
     * @return string
     */
    public function getClassTemplatePath()
    {
        return $this->classTemplatePath;
    }

    /**
     * getClassTemplateFile
     *
     * @return string
     */
    public function getClassTemplateFile()
    {
        return $this->classTemplateFile;
    }
}
