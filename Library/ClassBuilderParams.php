<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassBuilderParams
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class ClassBuilderParams
{

    /**
     * @var string
     */
    protected $baseClassName;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $parentClass;

    /**
     * create
     *
     * @param string      $className
     * @param string      $path
     * @param string      $namespace
     * @param string|null $parentClass
     *
     * @return ClassBuilderParams
     */
    public static function create(
        string $className,
        string $path,
        string $namespace,
        ?string $parentClass = null
    ) {
        $params = new self();

        $params->setPath($path);
        $params->setNamespace($namespace);
        $params->setBaseClassName($className);
        $params->setParentClass($parentClass);

        return $params;
    }

    /**
     * getBaseClassName
     *
     * @return string
     */
    public function getBaseClassName()
    {
        return $this->baseClassName;
    }

    /**
     * setBaseClassName
     *
     * @param string $baseClassName
     *
     * @return ClassBuilderParams
     */
    public function setBaseClassName(string $baseClassName)
    {
        $this->baseClassName = $baseClassName;

        return $this;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * setPath
     *
     * @param string $path
     *
     * @return ClassBuilderParams
     */
    public function setPath(string $path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * getNamespace
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * setNamespace
     *
     * @param string $namespace
     *
     * @return ClassBuilderParams
     */
    public function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * getParentClass
     *
     * @return string
     */
    public function getParentClass()
    {
        return $this->parentClass;
    }

    /**
     * setParentClass
     *
     * @param string $parentClass
     *
     * @return ClassBuilderParams
     */
    public function setParentClass(?string $parentClass)
    {
        $this->parentClass = $parentClass;

        return $this;
    }
}
