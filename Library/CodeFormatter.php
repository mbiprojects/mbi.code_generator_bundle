<?php

namespace Mbi\CodeGeneratorBundle\Library;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * CodeFormatter
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class CodeFormatter
{

    /**
     * formatFile
     *
     * @param string $filename
     * @param string $directory
     *
     * @return void
     * @throws \Exception
     */
    public function formatFile(string $filename, string $directory)
    {
        $file = $directory.DIRECTORY_SEPARATOR.$filename;
        if (!is_file($file)) {
            return;
        }

        //TODO: deprecated!?!
        $shellCommand = 'php '.$this->getCsFixerBinary().' fix -vv --config='.$this->getCsConfigFile().' '.$file;
        $process      = new Process($shellCommand);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }

    /**
     * getCsFixerBinary
     *
     * @return string
     * @throws \Exception
     */
    protected function getCsFixerBinary()
    {
        $dir = $this->getBinaryDirFromComposerJson();
        if (is_null($dir)) {
            $dir = './vendor/bin';
        }

        $binary = $dir.DIRECTORY_SEPARATOR.'php-cs-fixer';
        if (!file_exists($binary)) {
            throw new \Exception('php-cs-fixer binary not found');
        }

        return $binary;
    }

    /**
     * getBinaryDirFromComposerJson
     *
     * @return string
     */
    protected function getBinaryDirFromComposerJson()
    {
        $composerJson = './composer.json';
        if (!file_exists($composerJson)) {
            return null;
        }

        $file = file_get_contents($composerJson);
        $json = json_decode($file, true);

        $dir = null;
        if (is_array($json) && isset($json['config']['bin-dir'])) {
            $dir = $json['config']['bin-dir'];
        }

        return $dir;
    }

    /**
     * getCsConfigFile
     *
     * @return string
     */
    public function getCsConfigFile()
    {
        return __DIR__.DIRECTORY_SEPARATOR.'.php_cs';
    }
}
