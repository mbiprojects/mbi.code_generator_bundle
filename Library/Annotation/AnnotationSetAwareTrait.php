<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationSetAwareTrait
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
trait AnnotationSetAwareTrait
{

    /**
     * @var AnnotationSet
     */
    protected $annotationSet;

    /**
     * getAnnotationSet
     *
     * @return AnnotationSet
     */
    public function getAnnotationSet()
    {
        return $this->annotationSet;
    }

    /**
     * @param AnnotationSet $annotationSet
     *
     * @return AnnotationSetAwareTrait
     */
    public function setAnnotationSet(AnnotationSet $annotationSet)
    {
        $this->annotationSet = $annotationSet;

        return $this;
    }

    /**
     * hasAnnotations
     *
     * @return boolean
     */
    public function hasAnnotations()
    {
        if ($this->annotationSet) {
            return (bool)count($this->annotationSet->getAnnotations());
        }

        return false;
    }

    /**
     * getAnnotations
     *
     * @return AnnotationTemplate[]|array
     */
    public function getAnnotations()
    {
        if ($this->annotationSet) {
            return $this->annotationSet->getAnnotations();
        }

        return [];
    }
}
