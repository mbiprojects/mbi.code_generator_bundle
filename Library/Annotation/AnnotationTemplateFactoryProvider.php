<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationFactoryProvider
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class AnnotationTemplateFactoryProvider
{

    /**
     * @var AnnotationTemplateFactoryInterface[]
     */
    protected $factories;

    /**
     * registerFactory
     *
     * @param AnnotationTemplateFactoryInterface $factory
     *
     * @return AnnotationTemplateFactoryProvider
     */
    public function registerFactory(AnnotationTemplateFactoryInterface $factory)
    {
        $this->factories[] = $factory;

        return $this;
    }

    /**
     * getFactoryForInstruction
     *
     * @param AnnotationInstruction $instruction
     *
     * @return AnnotationTemplateFactoryInterface
     * @throws \Exception
     */
    public function getFactoryForInstruction(AnnotationInstruction $instruction)
    {
        foreach ($this->factories as $factory) {
            if ($factory->supportInstruction($instruction)) {
                return $factory;
            }
        }

        throw new \Exception('no factory found for instrucation');
    }
}
