<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationInstruction
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class BaseAnnotationInstruction extends AnnotationInstruction
{

    /**
     * @var string
     */
    protected $ident;

    /**
     * @var string
     */
    protected $value;

    /**
     * create
     *
     * @param string $ident
     * @param string $value
     *
     * @return BaseAnnotationInstruction
     */
    public static function createByIdentAndValue(string $ident, string $value)
    {
        $instruction        = new self();
        $instruction->ident = $ident;
        $instruction->value = $value;

        return $instruction;
    }

    /**
     * getIdent
     *
     * @return string
     */
    public function getIdent()
    {
        return $this->ident;
    }

    /**
     * getValue
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
