<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationTemplateFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class BaseAnnotationTemplateFactory extends AnnotationTemplateFactory implements AnnotationTemplateFactoryInterface
{

    /**
     * createByInstuction
     *
     * @param BaseAnnotationInstruction $instruction
     *
     * @return AnnotationTemplate
     */
    public function createByInstuction(AnnotationInstruction $instruction)
    {
        $annotation = $this->createNewAnnotationTemplateInstance();

        $annotation->setIdent($instruction->getIdent());
        $annotation->setValue($instruction->getValue());

        return $annotation;
    }

    /**
     * createNewAnnotationTemplateInstance
     *
     * @return BaseAnnotationTemplate
     */
    protected function createNewAnnotationTemplateInstance()
    {
        return new BaseAnnotationTemplate();
    }

    /**
     * createAnnotationSet
     *
     * @param AnnotationTemplate[] $annotationTemplates
     *
     * @return AnnotationSet
     */
    public function createAnnotationSet(array $annotationTemplates = [])
    {
        $annotationSet = $this->createNewAnnotationSetInstance();

        if (count($annotationTemplates)) {
            $annotationSet->setAnnotations($annotationTemplates);
        }

        return $annotationSet;
    }

    /**
     * createNewAnnotationSetInstance
     *
     * @return AnnotationSet
     */
    protected function createNewAnnotationSetInstance()
    {
        return new AnnotationSet();
    }

    /**
     * supportInstruction
     *
     * @param AnnotationInstruction $instruction
     *
     * @return boolean
     */
    public function supportInstruction(AnnotationInstruction $instruction)
    {
        return ($instruction instanceof BaseAnnotationInstruction);
    }
}
