<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationDecoratorPluginProvider
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class AnnotationDecoratorPluginProvider
{

    /**
     * @var AnnotationDecoratorPluginInterface[]
     */
    protected $plugins;

    /**
     * addPlugin
     *
     * @param AnnotationDecoratorPluginInterface $plugin
     *
     * @return AnnotationDecoratorPluginProvider
     */
    public function addPlugin(AnnotationDecoratorPluginInterface $plugin)
    {
        $this->plugins[$plugin->getIdent()] = $plugin;

        return $this;
    }

    /**
     * getPlugin
     *
     * @param string $ident
     *
     * @return AnnotationDecoratorPluginInterface
     * @throws \Exception
     */
    public function getPlugin(string $ident)
    {
        if (!array_key_exists($ident, $this->plugins)) {
            throw new \Exception('plugin '.$ident.' is not registered!');
        }

        return $this->plugins[$ident];
    }

    /**
     * getPlugins
     *
     * @return AnnotationDecoratorPluginInterface[]
     */
    public function getPlugins()
    {
        return $this->plugins;
    }
}
