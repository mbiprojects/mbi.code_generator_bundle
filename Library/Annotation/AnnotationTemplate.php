<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationTemplate
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class AnnotationTemplate
{

    const IDENT_RETURN = 'return';
    const IDENT_VAR = 'var';

    /**
     * @var string
     */
    protected $ident;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var integer
     */
    protected $orderIndex = 1;

    /**
     * getIdent
     *
     * @return string
     */
    public function getIdent()
    {
        return $this->ident;
    }

    /**
     * @param string $ident
     *
     * @return AnnotationTemplate
     */
    public function setIdent(string $ident): AnnotationTemplate
    {
        $this->ident = $ident;

        return $this;
    }

    /**
     * getValue
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return AnnotationTemplate
     */
    public function setValue(string $value): AnnotationTemplate
    {
        $this->value = $value;

        return $this;
    }

    /**
     * getOrderIndex
     *
     * @return int
     */
    public function getOrderIndex()
    {
        return $this->orderIndex;
    }

    /**
     * setOrderIndex
     *
     * @param int $orderIndex
     *
     * @return AnnotationTemplate
     */
    public function setOrderIndex($orderIndex)
    {
        $this->orderIndex = $orderIndex;

        return $this;
    }

    /**
     * render
     *
     * @return string
     */
    public function render()
    {
        return '@'.$this->getIdent().' '.$this->getValue();
    }
}
