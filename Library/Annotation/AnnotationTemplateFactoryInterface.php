<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationTemplateFactoryInterface
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
interface AnnotationTemplateFactoryInterface
{

    /**
     * createByInstuction
     *
     * @param AnnotationInstruction $instruction
     *
     * @return AnnotationTemplate
     */
    public function createByInstuction(AnnotationInstruction $instruction);

    /**
     * supportInstruction
     *
     * @param AnnotationInstruction $instruction
     *
     * @return boolean
     */
    public function supportInstruction(AnnotationInstruction $instruction);
}
