<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationOrderDecorator
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class AnnotationOrderDecorator
{

    /**
     * @var array
     */
    private $identToOrderIndexMapping = [
        'var'      => 1,
        'return'   => 9999,
        'doctrine' => 2,
    ];

    /**
     * assignOrderIndex
     *
     * @param AnnotationSetAwareInterface $template
     *
     * @return AnnotationOrderDecorator
     */
    public function applyOrder(AnnotationSetAwareInterface $template)
    {
        if (!$template->hasAnnotations()) {
            return $this;
        }

        $annotations = $template->getAnnotations();
        foreach ($annotations as $annotation) {
            $this->decorateOrderIndex($annotation);
        }

        $annotations = $this->applyIndex($annotations);
        $template->getAnnotationSet()->setAnnotations($annotations);

        return $this;
    }

    /**
     * decorateOrderIndex
     *
     * @param AnnotationTemplate $annotation
     *
     * @return void
     */
    private function decorateOrderIndex(AnnotationTemplate $annotation)
    {
        if (array_key_exists($annotation->getIdent(), $this->identToOrderIndexMapping)
        ) {
            $annotation->setOrderIndex($this->identToOrderIndexMapping[$annotation->getIdent()]);
        }
    }

    /**
     * applyIndex
     *
     * @param AnnotationTemplate[] $annotations
     *
     * @return array
     */
    private function applyIndex(array $annotations)
    {
        $annotationsByIndexGroup = [];
        foreach ($annotations as $index => $annotation) {
            $annotationsByIndexGroup[$annotation->getOrderIndex()][] = $annotation;
        }

        uksort(
            $annotationsByIndexGroup,
            function ($indexA, $indexB) {
                return ($indexA > $indexB) ? true : false;
            }
        );

        $result = [];
        foreach ($annotationsByIndexGroup as $index => $annotations) {
            $result = array_merge($result, $annotations);
        }

        return $result;
    }
}
