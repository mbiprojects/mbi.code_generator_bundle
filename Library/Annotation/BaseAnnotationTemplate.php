<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * BaseAnnotationTemplate
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class BaseAnnotationTemplate extends AnnotationTemplate
{

}
