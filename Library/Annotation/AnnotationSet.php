<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationSet
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class AnnotationSet
{

    /**
     * @var AnnotationTemplate[]
     */
    protected $annotations = [];

    /**
     * addTemplate
     *
     * @param AnnotationTemplate $annotationTemplate
     *
     * @return AnnotationSet
     */
    public function addTemplate(AnnotationTemplate $annotationTemplate)
    {
        $this->annotations[] = $annotationTemplate;

        return $this;
    }

    /**
     * getAnnotations
     *
     * @return AnnotationTemplate[]
     */
    public function getAnnotations()
    {
        return $this->annotations;
    }

    /**
     * @param AnnotationTemplate[] $annotations
     *
     * @return AnnotationSet
     */
    public function setAnnotations(array $annotations): AnnotationSet
    {
        $this->annotations = $annotations;

        return $this;
    }

    /**
     * addAnnotations
     *
     * @param array $annotations
     *
     * @return $this
     */
    public function addAnnotations(array $annotations)
    {
        foreach ($annotations as $annotation) {
            $this->annotations[] = $annotation;
        }

        return $this;
    }
}
