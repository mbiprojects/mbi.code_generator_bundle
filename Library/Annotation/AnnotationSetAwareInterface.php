<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

/**
 * AnnotationSetAwareInterface
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
interface AnnotationSetAwareInterface
{

    /**
     * getAnnotationSet
     *
     * @return AnnotationSet
     */
    public function getAnnotationSet();

    /**
     * @param AnnotationSet $annotationSet
     *
     * @return AnnotationSetAwareTrait
     */
    public function setAnnotationSet(AnnotationSet $annotationSet);

    /**
     * getAnnotations
     *
     * @return AnnotationTemplate[]
     */
    public function getAnnotations();

    /**
     * hasAnnotations
     *
     * @return boolean
     */
    public function hasAnnotations();
}
