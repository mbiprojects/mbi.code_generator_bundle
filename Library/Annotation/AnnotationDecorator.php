<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\GetterTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\SetterTemplate;

/**
 * AnnotationDecorator
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class AnnotationDecorator
{

    /**
     * @var BaseAnnotationTemplateFactory
     */
    private $annotationTemplateFactory;

    /**
     * @var AnnotationTemplateFactoryProvider
     */
    private $factoryProvider;

    /**
     * @var AnnotationDecoratorPluginProvider
     */
    private $decoratorPluginProvider;

    /**
     * @var AnnotationOrderDecorator
     */
    private $annotationOrderDecorator;

    /**
     * __construct
     *
     * @param AnnotationTemplateFactoryProvider $factoryProvider
     * @param BaseAnnotationTemplateFactory     $annotationTemplateFactory
     * @param AnnotationDecoratorPluginProvider $decoratorPluginProvider
     * @param AnnotationOrderDecorator          $annotationOrderDecorator
     */
    public function __construct(
        AnnotationTemplateFactoryProvider $factoryProvider,
        BaseAnnotationTemplateFactory $annotationTemplateFactory,
        AnnotationDecoratorPluginProvider $decoratorPluginProvider,
        AnnotationOrderDecorator $annotationOrderDecorator
    ) {
        $this->annotationTemplateFactory = $annotationTemplateFactory;
        $this->factoryProvider           = $factoryProvider;
        $this->decoratorPluginProvider   = $decoratorPluginProvider;
        $this->annotationOrderDecorator  = $annotationOrderDecorator;
    }

    /**
     * addAnnotationToTemplate
     *
     * @param AnnotationSetAwareInterface $template
     * @param AnnotationInstruction       $instruction
     *
     * @return AnnotationDecorator
     */
    public function addAnnotationToTemplate(
        AnnotationSetAwareInterface $template,
        AnnotationInstruction $instruction
    ) {
        $annotationTemplate = $this->factoryProvider->getFactoryForInstruction($instruction)
            ->createByInstuction($instruction);

        $this->getAnnotationSet($template)->addTemplate($annotationTemplate);

        return $this;
    }

    /**
     * getAnnotationSet
     *
     * @param AnnotationSetAwareInterface $template
     *
     * @return AnnotationSet
     */
    protected function getAnnotationSet(AnnotationSetAwareInterface $template)
    {
        if (!$template->getAnnotationSet()) {
            $template->setAnnotationSet($this->annotationTemplateFactory->createAnnotationSet());
        }

        return $template->getAnnotationSet();
    }

    /**
     * decorate
     *
     * @param MethodTemplate $methodTemplate
     *
     * @return AnnotationDecorator
     */
    public function decorateSignitureAnnotations(MethodTemplate $methodTemplate)
    {
        foreach ($methodTemplate->getArguments() as $argument) {
            $this->addAnnotationToTemplate(
                $methodTemplate,
                BaseAnnotationInstruction::createByIdentAndValue(
                    'param',
                    trim($argument->getSignatureTypehint().' $'.$argument->getName())
                )
            );
        }

        return $this;
    }

    /**
     * decorateGetterSetterAnnotations
     *
     * @param MethodTemplate $methodTemplate
     *
     * @return AnnotationDecorator
     */
    public function decorateGetterSetterAnnotations(MethodTemplate $methodTemplate)
    {
        if ($methodTemplate instanceof GetterTemplate) {
            $this->addAnnotationToTemplate(
                $methodTemplate,
                BaseAnnotationInstruction::createByIdentAndValue(
                    AnnotationTemplate::IDENT_RETURN,
                    $methodTemplate->getPropertyTemplate()->getDataTypeSignature()
                )
            );
        }

        if ($methodTemplate instanceof SetterTemplate) {
            $this->addAnnotationToTemplate(
                $methodTemplate,
                BaseAnnotationInstruction::createByIdentAndValue(
                    AnnotationTemplate::IDENT_RETURN,
                    '$this'
                )
            );
        }

        return $this;
    }

    /**
     * executeDecoratorPlugins
     *
     * @param ClassTemplate $classTemplate
     *
     * @return $this
     */
    public function executeDecoratorPluginsOnClassTemplate(ClassTemplate $classTemplate)
    {
        foreach ($this->decoratorPluginProvider->getPlugins() as $plugin) {
            $plugin->decorate($classTemplate);
        }

        return $this;
    }

    /**
     * orderAnnotations
     *
     * @param ClassTemplate $template
     *
     * @return AnnotationDecorator
     */
    public function orderAnnotations(ClassTemplate $template)
    {
        //on class
        $this->annotationOrderDecorator->applyOrder($template);

        //on properties
        foreach ($template->getProperties() as $property) {
            $this->annotationOrderDecorator->applyOrder($property);
        }

        //on methods
        foreach ($template->getMethods() as $method) {
            $this->annotationOrderDecorator->applyOrder($method);
        }

        return $this;
    }
}
