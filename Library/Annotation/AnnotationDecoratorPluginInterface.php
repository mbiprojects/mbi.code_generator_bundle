<?php

namespace Mbi\CodeGeneratorBundle\Library\Annotation;

use Mbi\CodeGeneratorBundle\Library\ClassTemplate;

/**
 * AnnotationDecoratorPluginInterface
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
interface AnnotationDecoratorPluginInterface
{

    /**
     * getIdent
     *
     * @return string
     */
    public function getIdent();

    /**
     * decorate
     *
     * @param ClassTemplate $classTemplate
     *
     * @return void
     */
    public function decorate(ClassTemplate $classTemplate);
}
