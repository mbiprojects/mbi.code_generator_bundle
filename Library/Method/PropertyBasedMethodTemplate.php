<?php

namespace Mbi\CodeGeneratorBundle\Library\Method;

use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;

/**
 * PropertyBasedMethodTemplate
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class PropertyBasedMethodTemplate extends MethodTemplate
{

    /**
     * @var PropertyTemplate
     */
    protected $propertyTemplate;

    /**
     * getPropertyTemplate
     *
     * @return PropertyTemplate
     */
    public function getPropertyTemplate()
    {
        return $this->propertyTemplate;
    }

    /**
     * @param PropertyTemplate $propertyTemplate
     *
     * @return MethodTemplate
     */
    public function setPropertyTemplate(PropertyTemplate $propertyTemplate): MethodTemplate
    {
        $this->propertyTemplate = $propertyTemplate;

        return $this;
    }
}
