<?php

namespace Mbi\CodeGeneratorBundle\Library\Method;

use Mbi\CodeGeneratorBundle\Library\DataType\DataType;

/**
 * ArgumentTemplateFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ArgumentTemplateFactory
{

    /**
     * createTemplate
     *
     * @param string   $name
     * @param DataType $dataType
     * @param array    $options
     *
     * @return ArgumentTemplate
     */
    public function createTemplate(string $name, DataType $dataType, array $options = [])
    {
        $argumentTemplate = $this->createNewInstance();

        $argumentTemplate->setName($name);
        $argumentTemplate->setDataType($dataType);
        $argumentTemplate->setOptions($options);

        return $argumentTemplate;
    }

    /**
     * getClassTemplate
     *
     * @return ArgumentTemplate
     */
    protected function createNewInstance()
    {
        return new ArgumentTemplate();
    }
}
