<?php

namespace Mbi\CodeGeneratorBundle\Library\Method;

use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\DataType\DataTypeFactory;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;

/**
 * MethodTemplateFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class MethodTemplateFactory
{

    /**
     * @var ArgumentTemplateFactory
     */
    private $argumentTemplateFactory;

    /**
     * @var DataTypeFactory
     */
    private $dataTypeFactory;

    /**
     * __construct
     *
     * @param ArgumentTemplateFactory $argumentTemplateFactory
     * @param DataTypeFactory         $dataTypeFactory
     */
    public function __construct(ArgumentTemplateFactory $argumentTemplateFactory, DataTypeFactory $dataTypeFactory)
    {
        $this->argumentTemplateFactory = $argumentTemplateFactory;
        $this->dataTypeFactory         = $dataTypeFactory;
    }

    /**
     * create
     *
     * @param MethodTemplate        $methodTemplate
     * @param string                $methodName
     * @param ArgumentTemplate[]    $arguments
     * @param PropertyTemplate|null $propertyTemplate
     *
     * @return void
     */
    protected function initMethod(
        MethodTemplate $methodTemplate,
        string $methodName,
        array $arguments,
        ?PropertyTemplate $propertyTemplate = null
    ) {
        $methodTemplate->setName($methodName);

        if ($propertyTemplate instanceof PropertyTemplate
            && $methodTemplate instanceof PropertyBasedMethodTemplate
        ) {
            $methodTemplate->setPropertyTemplate($propertyTemplate);
        }

        $methodTemplate->setArguments($arguments);
    }

    /**
     * createGetter
     *
     * @param PropertyTemplate $propertyTemplate
     *
     * @return GetterTemplate
     */
    public function createGetter(PropertyTemplate $propertyTemplate)
    {
        $instance = new GetterTemplate();

        $methodName = $this->getMethodNameByPropertyNameAndPrefix($propertyTemplate->getName(), 'get');
        $this->initMethod($instance, $methodName, [], $propertyTemplate);

        return $instance;
    }

    /**
     * createSetter
     *
     * @param PropertyTemplate $propertyTemplate
     *
     * @return SetterTemplate
     */
    public function createSetter(PropertyTemplate $propertyTemplate)
    {
        $instance = new SetterTemplate();

        $methodName = $this->getMethodNameByPropertyNameAndPrefix($propertyTemplate->getName(), 'set');
        $arguments  = [$this->createArgumentFromTemplate($propertyTemplate)];
        $this->initMethod($instance, $methodName, $arguments, $propertyTemplate);

        return $instance;
    }

    /**
     * createArgumentFromTemplate
     *
     * @param IsArgumentableTemplateInterface $argumentableTemplate
     *
     * @return ArgumentTemplate
     */
    public function createArgumentFromTemplate(IsArgumentableTemplateInterface $argumentableTemplate)
    {
        return $this->createArgument(
            $argumentableTemplate->getName(),
            $argumentableTemplate->getDataType(),
            $argumentableTemplate->getOptions()
        );
    }

    /**
     * createArgument
     *
     * @param string   $argumentName
     * @param DataType $dataType
     * @param array    $options
     *
     * @return ArgumentTemplate
     */
    public function createArgument(
        string $argumentName,
        DataType $dataType,
        array $options = []
    ) {
        return $this->argumentTemplateFactory->createTemplate(
            $argumentName,
            $dataType,
            $options
        );
    }

    /**
     * createNewInstance
     *
     * @return MethodTemplate
     */
    public function createNewInstance()
    {
        return new MethodTemplate();
    }

    /**
     * getMethodNameByPropertyNameAndPrefix
     *
     * @param string $propertyName
     * @param string $prefix
     *
     * @return string
     */
    protected function getMethodNameByPropertyNameAndPrefix(string $propertyName, string $prefix)
    {
        return $prefix.ucfirst($propertyName);
    }
}
