<?php

namespace Mbi\CodeGeneratorBundle\Library\Method;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationSetAwareInterface;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationSetAwareTrait;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplate;

/**
 * MethodTemplate
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class MethodTemplate implements AnnotationSetAwareInterface
{

    use AnnotationSetAwareTrait;

    const VISIBILITY_PUBLIC = 'public';
    const VISIBILITY_PROTECTED = 'protected';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $visibility = self::VISIBILITY_PUBLIC;

    /**
     * @var ArgumentTemplate[]
     */
    protected $arguments = [];

    /**
     * @var \Closure
     */
    protected $contentCallback;

    /**
     * addArgument
     *
     * @param ArgumentTemplate $argument
     *
     * @return MethodTemplate
     */
    public function addArgument(ArgumentTemplate $argument)
    {
        $this->arguments[] = $argument;

        return $this;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return MethodTemplate
     */
    public function setName(string $name): MethodTemplate
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getArguments
     *
     * @return ArgumentTemplate[]
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param ArgumentTemplate[] $arguments
     *
     * @return MethodTemplate
     */
    public function setArguments(array $arguments): MethodTemplate
    {
        $this->arguments = $arguments;

        return $this;
    }

    /**
     * getArgumentList
     *
     * @return array
     */
    public function getArgumentList()
    {
        $arguments = [];
        if (count($this->getArguments())) {
            foreach ($this->getArguments() as $argument) {
                $arguments[] = ($argument->isNullable() ? '?' : '').
                    trim($argument->getSignatureTypehint().' $'.$argument->getName());
            }
        }

        return $arguments;
    }

    /**
     * getMethodAnnotations
     *
     * @return array|AnnotationTemplate[]
     */
    public function getMethodAnnotations()
    {
        if ($this->getAnnotationSet()) {
            return $this->getAnnotationSet()->getAnnotations();
        }

        return [];
    }

    /**
     * isGetter
     *
     * @return bool
     */
    public function isGetter()
    {
        return $this instanceof GetterTemplate;
    }

    /**
     * isSetter
     *
     * @return bool
     */
    public function isSetter()
    {
        return $this instanceof SetterTemplate;
    }

    /**
     * getVisibility
     *
     * @return string
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     *
     * @return MethodTemplate
     */
    public function setVisibility(string $visibility): MethodTemplate
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * addContentCallback
     *
     * @param \Closure $function
     *
     * @return $this
     */
    public function addContentCallback(\Closure $function)
    {
        $this->contentCallback = $function;

        return $this;
    }

    /**
     * getContentCallback
     *
     * @return \Closure|null
     */
    public function getContentCallback()
    {
        return $this->contentCallback;
    }

    /**
     * hasContentCallback
     *
     * @return bool
     */
    public function hasContentCallback()
    {
        return !is_null($this->getContentCallback());
    }

    /**
     * executeContentCallback
     *
     * @return mixed
     */
    public function executeContentCallback()
    {
        return call_user_func($this->contentCallback);
    }
}
