<?php

namespace Mbi\CodeGeneratorBundle\Library\Method;

use Mbi\CodeGeneratorBundle\Library\DataType\DataType;

/**
 * IsArgumentableTemplateInterface
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
interface IsArgumentableTemplateInterface
{

    /**
     * getName
     *
     * @return string
     */
    public function getName();

    /**
     * getDataType
     *
     * @return DataType
     */
    public function getDataType();

    /**
     * isNullable
     *
     * @return boolean
     */
    public function isNullable();

    /**
     * getOptions
     *
     * @return array
     */
    public function getOptions();
}
