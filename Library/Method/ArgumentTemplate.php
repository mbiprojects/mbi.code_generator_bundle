<?php

namespace Mbi\CodeGeneratorBundle\Library\Method;

use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;

/**
 * ArgumentTemplate
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ArgumentTemplate
{

    const OPTION_NULLABLE = 'nullable';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var DataType
     */
    protected $dataType;

    /**
     * @var array
     */
    protected $options;

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ArgumentTemplate
     */
    public function setName(string $name): ArgumentTemplate
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getDataType
     *
     * @return DataType
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * getSignatureTypehint
     *
     * @return string
     */
    public function getSignatureTypehint()
    {
        return ($this->dataType ? $this->dataType->getTypeHint() : '');
    }

    /**
     * @param DataType $dataType
     *
     * @return ArgumentTemplate
     */
    public function setDataType(DataType $dataType): ArgumentTemplate
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * getNullable
     *
     * @return bool
     */
    public function isNullable()
    {
        if (array_key_exists(self::OPTION_NULLABLE, $this->options)) {
            return $this->options[self::OPTION_NULLABLE];
        }

        return null;
    }

    /**
     * @param bool $nullable
     *
     * @return PropertyTemplate
     */
    public function setNullable(bool $nullable): PropertyTemplate
    {
        $this->options[self::OPTION_NULLABLE] = $nullable;

        return $this;
    }

    /**
     * setOptions
     *
     * @param array $options
     *
     * @return void
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * getOptions
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }
}
