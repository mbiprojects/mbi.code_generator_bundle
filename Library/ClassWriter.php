<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassWriter
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassWriter
{

    /**
     * @var CodeFormatter
     */
    private $formatter;

    /**
     * __construct
     *
     * @param CodeFormatter $formatter
     */
    public function __construct(CodeFormatter $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * writeToFile
     *
     * @param string $targetPath
     * @param string $fileName
     * @param string $content
     *
     * @return ClassWriter
     */
    public function writeToFile(string $targetPath, string $fileName, string $content)
    {
        $this->createTargetDirectory($targetPath);

        $file = $targetPath.DIRECTORY_SEPARATOR.$fileName;

        file_put_contents($file, $content);

        $this->formatter->formatFile($fileName, $targetPath);

        return $this;
    }

    /**
     * createTargetDirectory
     *
     * @param string $path
     *
     * @return void
     */
    protected function createTargetDirectory(string $path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
    }
}
