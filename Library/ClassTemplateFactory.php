<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassTemplateFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassTemplateFactory
{

    /**
     * @var ClassProxyFactory
     */
    private $classProxyFactory;

    /**
     * __construct
     *
     * @param ClassProxyFactory $classProxyFactory
     */
    public function __construct(ClassProxyFactory $classProxyFactory)
    {
        $this->classProxyFactory = $classProxyFactory;
    }

    /**
     * createTemplate
     *
     * @param string      $baseClassName
     * @param string      $path
     * @param string      $namespace
     * @param string|null $parentClass
     *
     * @return ClassTemplate
     */
    public function createTemplate(string $baseClassName, string $path, string $namespace, ?string $parentClass = null)
    {
        $template   = $this->createNewInstance();
        $classProxy = $this->classProxyFactory->createByBaseNameAndNamespace($baseClassName, $namespace, $path);
        $template->setClassProxy($classProxy);

        if ($parentClass) {
            $template->setParentClassProxy($this->classProxyFactory->createClassProxy($parentClass));
        }

        return $template;
    }

    /**
     * createFromExistingClass
     *
     * @param string $fqcn
     *
     * @return ClassTemplate
     * @throws \Exception
     */
    public function createFromExistingClass(string $fqcn)
    {
        if (!class_exists($fqcn)) {
            throw new \Exception('class '.$fqcn.' does not exist!');
        }
        $template = $this->createNewInstance();

        $template->setClassProxy($this->classProxyFactory->createClassProxy($fqcn));
        $template->setParentClassProxy($this->classProxyFactory->createParentClassProxy($fqcn));

        //add interfaces
        $interfaceProxies = $this->classProxyFactory->getUsedInterfaceClassProxies($fqcn);
        if (count($interfaceProxies)) {
            foreach ($interfaceProxies as $interfaceProxy) {
                $template->addInterface($interfaceProxy);
            }
        }

        //add traits
        $traitProxies = $this->classProxyFactory->getUsedTraitClassProxies($fqcn);
        if (count($traitProxies)) {
            foreach ($traitProxies as $traitProxy) {
                $template->addTrait($traitProxy);
            }
        }

        //get properties


        return $template;
    }

    /**
     * getClassTemplate
     *
     * @return ClassTemplate
     */
    protected function createNewInstance()
    {
        return new ClassTemplate();
    }

    /**
     * getParentClassProxy
     *
     * @param string $parentClass
     *
     * @return ClassProxy|null
     * @throws \Exception
     */
    protected function getParentClassProxy(?string $parentClass)
    {
        if ($parentClass) {
            if (!class_exists($parentClass)) {
                throw new \Exception('invalid parent class given');
            }

            return $this->classProxyFactory->createClassProxy($parentClass);
        }

        return null;
    }
}
