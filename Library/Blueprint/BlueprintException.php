<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

/**
 * BlueprintException
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class BlueprintException extends \Exception
{

}
