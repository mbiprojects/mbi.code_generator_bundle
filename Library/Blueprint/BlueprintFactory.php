<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

use Mbi\CodeGeneratorBundle\Library\ClassBuilderFactory;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;

/**
 * BluePrintFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class BlueprintFactory
{

    /**
     * @var ClassBuilderFactory
     */
    private $classBuilderFactory;

    /**
     * __construct
     *
     * @param ClassBuilderFactory $classBuilderFactory
     */
    public function __construct(ClassBuilderFactory $classBuilderFactory)
    {
        $this->classBuilderFactory = $classBuilderFactory;
    }

    /**
     * createBlueprint
     *
     * @param string                  $blueprintClass
     * @param BlueprintSettings|null  $settings
     *
     * @param ClassTemplateStack|null $templateStack
     *
     * @return AbstractBlueprint
     * @throws BlueprintException
     */
    public function createBlueprint(
        string $blueprintClass,
        BlueprintSettings $settings = null,
        ClassTemplateStack $templateStack = null
    ) {
        if (!class_exists($blueprintClass)) {
            throw new BlueprintException('blueprint-class '.$blueprintClass.' does not exist!');
        }

        if (!is_subclass_of($blueprintClass, AbstractBlueprint::class)) {
            throw new BlueprintException('blueprint-class needs to extend '.AbstractBlueprint::class);
        }

        return new $blueprintClass($this->classBuilderFactory, $settings, $templateStack);
    }
}
