<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;

/**
 * BlueprintProcessor
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class BlueprintProcessor
{

    /**
     * @var BlueprintFactory
     */
    private $blueprintFactory;

    /**
     * @var BlueprintStackFactory
     */
    private $blueprintStackFactory;

    /**
     * @var BlueprintSettingsFactory
     */
    private $settingsFactory;

    /**
     * __construct
     *
     * @param BlueprintFactory         $blueprintFactory
     * @param BlueprintStackFactory    $blueprintStackFactory
     * @param BlueprintSettingsFactory $settingsFactory
     */
    public function __construct(
        BlueprintFactory $blueprintFactory,
        BlueprintStackFactory $blueprintStackFactory,
        BlueprintSettingsFactory $settingsFactory
    ) {
        $this->blueprintFactory      = $blueprintFactory;
        $this->blueprintStackFactory = $blueprintStackFactory;
        $this->settingsFactory       = $settingsFactory;
    }

    /**
     * createSettings
     *
     * @return BlueprintSettings
     */
    public function createSettings()
    {
        return $this->settingsFactory->createSettings();
    }

    /**
     * createEntity
     *
     * @param string                  $blueprintClass
     * @param string                  $className
     * @param string                  $path
     * @param string                  $namespace
     * @param BlueprintSettings|null  $settings
     * @param ClassTemplateStack|null $templateStack
     *
     * @return ClassTemplate
     */
    public function generate(
        string $blueprintClass,
        string $className,
        string $path,
        string $namespace,
        BlueprintSettings $settings = null,
        ClassTemplateStack $templateStack = null
    ) {
        if (is_null($settings)) {
            $settings = $this->createSettings();
        }
        $blueprint = $this->blueprintFactory->createBlueprint($blueprintClass, $settings, $templateStack);

        return $blueprint->initBuild($className, $path, $namespace)->persist();
    }

    /**
     * generateByStack
     *
     * @param string                 $blueprintStackClass
     * @param string                 $package
     * @param string                 $packagePath
     * @param string                 $packageNamespace
     *
     * @param BlueprintSettings|null $settings
     *
     * @return ClassTemplateStack
     * @throws \Exception
     */
    public function generateByStack(
        string $blueprintStackClass,
        string $package,
        string $packagePath,
        string $packageNamespace,
        BlueprintSettings $settings = null
    ) {
        $stack = $this->blueprintStackFactory->createStack($blueprintStackClass);
        $stack->init($package);

        $templateStack = ClassTemplateStack::create();
        foreach ($stack->getItems() as $item) {
            $generatedTemplate = $this->generate(
                $item->getBlueprintClass(),
                $item->getClassName(),
                $packagePath,
                $packageNamespace,
                $settings,
                $templateStack
            );

            $templateStack->add($generatedTemplate);
        }

        return $templateStack;
    }
}
