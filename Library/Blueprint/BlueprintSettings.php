<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Annotation\BaseAnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;

/**
 * BlueprintSettings
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class BlueprintSettings
{

    /**
     * @var string
     */
    protected $author;

    /**
     * @var string
     */
    protected $license;

    /**
     * @var BaseAnnotationInstruction[]
     */
    protected $defaultClassAnnotations = [];

    /**
     * @var PropertyInstructionContainer[]
     */
    protected $propertyInstructions = [];

    /**
     * create
     *
     * @return BlueprintSettings
     */
    public static function create()
    {
        return new self();
    }

    /**
     * addPropertyInstructions
     *
     * @param PropertyInstructionContainer $propertyInstructionContainer
     *
     * @return BlueprintSettings
     */
    public function addPropertyInstructions(PropertyInstructionContainer $propertyInstructionContainer)
    {
        $this->propertyInstructions[$propertyInstructionContainer->getTag()] = $propertyInstructionContainer;

        return $this;
    }

    /**
     * getPropertyInstructionForTag
     *
     * @param string $tag
     *
     * @return PropertyInstructionContainer|null
     */
    public function getPropertyInstructionsForTag(string $tag)
    {
        if (!count($this->propertyInstructions)) {
            return null;
        }

        foreach ($this->propertyInstructions as $instructionContainer) {
            if ($instructionContainer->getTag() == $tag) {
                return $instructionContainer;
            }
        }

        return null;
    }

    /**
     * getDefaultClassAnnotations
     *
     * @return BaseAnnotationInstruction[]
     */
    public function getDefaultClassAnnotations()
    {
        return $this->defaultClassAnnotations;
    }

    /**
     * setDefaultClassAnnotations
     *
     * @param BaseAnnotationInstruction[] $defaultClassAnnotations
     *
     * @return BlueprintSettings
     */
    public function setDefaultClassAnnotations(array $defaultClassAnnotations)
    {
        $this->defaultClassAnnotations = $defaultClassAnnotations;

        return $this;
    }
}
