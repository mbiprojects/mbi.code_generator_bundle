<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

/**
 * BlueprintStackFactory
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class BlueprintStackFactory
{

    /**
     * createStack
     *
     * @param string $blueprintStackClass
     *
     * @return BlueprintStack
     * @throws \Exception
     */
    public function createStack(string $blueprintStackClass)
    {
        return $this->createNewInstance($blueprintStackClass);
    }

    /**
     * createNewInstance
     *
     * @param string $blueprintStackClass
     *
     * @return BlueprintStack
     * @throws \Exception
     */
    protected function createNewInstance(string $blueprintStackClass)
    {
        if (!is_subclass_of($blueprintStackClass, BlueprintStack::class)) {
            throw new \Exception('invalid blueprint stack given: '.$blueprintStackClass);
        }

        return new $blueprintStackClass();
    }
}
