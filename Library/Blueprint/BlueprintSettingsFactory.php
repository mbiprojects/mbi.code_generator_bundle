<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Annotation\BaseAnnotationInstruction;

/**
 * BlueprintSettingsFactory
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class BlueprintSettingsFactory
{

    /**
     * @var array
     */
    protected $defaultClassAnnotations = [];

    /**
     * __construct
     *
     * @param array $defaultClassAnnotations
     */
    public function __construct(array $defaultClassAnnotations = [])
    {
        $this->defaultClassAnnotations = $defaultClassAnnotations;
    }

    /**
     * createSettings
     *
     * @return BlueprintSettings
     */
    public function createSettings()
    {
        $settings = $this->createNewInstance();

        $this->loadDefaults($settings);

        return $settings;
    }

    /**
     * loadDefaults
     *
     * @param BlueprintSettings $settings
     *
     * @return BlueprintSettingsFactory
     */
    protected function loadDefaults(BlueprintSettings $settings)
    {
        $instructions = [];
        foreach ($this->defaultClassAnnotations as $key => $value) {
            $instructions[] = BaseAnnotationInstruction::createByIdentAndValue($key, $value);
        }

        $settings->setDefaultClassAnnotations($instructions);

        return $this;
    }

    /**
     * createNewInstance
     *
     * @return BlueprintSettings
     */
    protected function createNewInstance()
    {
        return new BlueprintSettings();
    }
}
