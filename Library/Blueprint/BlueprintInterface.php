<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

use Mbi\CodeGeneratorBundle\Library\ClassBuilder;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;

/**
 * BlueprintInterface
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
interface BlueprintInterface
{

    /**
     * build
     *
     * @param string $name
     * @param string $path
     * @param string $namespace
     *
     * @return $this
     */
    public function build(
        string $name,
        string $path,
        string $namespace
    );

    /**
     * setTemplateStack
     *
     * @param ClassTemplateStack|null $templateStack
     *
     * @return $this
     */
    public function setTemplateStack(?ClassTemplateStack $templateStack);

    /**
     * getClassBuilder
     *
     * @return ClassBuilder
     */
    public function getClassBuilder();
}
