<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Annotation\BaseAnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\ClassBuilder;
use Mbi\CodeGeneratorBundle\Library\ClassBuilderFactory;
use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;

/**
 * AbstractBlueprint
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
abstract class AbstractBlueprint implements BlueprintInterface
{

    /**
     * @var ClassBuilderFactory
     */
    protected $classBuilderFactory;

    /**
     * @var ClassBuilder
     */
    private $classBuilder;

    /**
     * @var ClassTemplateStack|null
     */
    protected $templateStack;

    /**
     * @var BlueprintSettings
     */
    protected $settings;

    /**
     * __construct
     *
     * @param ClassBuilderFactory     $classBuilderFactory
     * @param BlueprintSettings       $settings
     * @param ClassTemplateStack|null $templateStack
     */
    public function __construct(
        ClassBuilderFactory $classBuilderFactory,
        BlueprintSettings $settings = null,
        ClassTemplateStack $templateStack = null
    ) {
        $this->classBuilderFactory = $classBuilderFactory;
        $this->settings            = $settings;
        $this->templateStack       = $templateStack;
    }

    /**
     * initClassBuilder
     *
     * @param string      $baseClassName
     * @param string      $path
     * @param string      $namespace
     * @param string|null $parentClass
     *
     * @return void
     */
    public function initClassBuilder(
        string $baseClassName,
        string $path,
        string $namespace,
        ?string $parentClass = null
    ) {
        //init class builder
        $this->classBuilder = $this->classBuilderFactory->createBuilder();
        $this->classBuilder->initClassTemplate(
            $baseClassName,
            $path,
            $namespace,
            $parentClass
        );
    }

    /**
     * initBuild
     *
     * @param string $className
     * @param string $path
     * @param string $namespace
     *
     * @return $this
     */
    public function initBuild(
        string $className,
        string $path,
        string $namespace
    ) {
        $this->build($className, $path, $namespace);

        $this->getClassBuilder()->decorateAnnotations($this->settings->getDefaultClassAnnotations());

        return $this;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getClassBuilder()
    {
        if (is_null($this->classBuilder)) {
            throw new \Exception('classBuilder not initialized yet!');
        }

        return $this->classBuilder;
    }

    /**
     * @inheritDoc
     */
    public function setTemplateStack(?ClassTemplateStack $templateStack)
    {
        $this->templateStack = $templateStack;

        return $this;
    }

    /**
     * getTemplateStack
     *
     * @return ClassTemplateStack
     */
    public function getTemplateStack()
    {
        return $this->templateStack;
    }

    /**
     * persist
     *
     * @return ClassTemplate
     */
    public function persist()
    {
        return $this->getClassBuilder()->render()->writeToFile();
    }

    /**
     * initConstructor
     *
     * @return MethodTemplate
     */
    protected function initConstructor()
    {
        $constructorArgument = $this->classBuilder->getConstructorArguments();

        return $this->classBuilder->createMethod(
            '__construct',
            [],
            $constructorArgument,
            function () use ($constructorArgument) {
                $code = '';
                $ws   = '        ';
                foreach ($constructorArgument as $index => $argument) {
                    $code .= $ws.'$this->'.$argument->getName().' = $'.$argument->getName().';';
                    if ($index != count($constructorArgument) - 1) {
                        $code .= "\n";
                    }
                }

                return $code;
            }
        );
    }

    /**
     * getStackTemplate
     *
     * @param string $tag
     *
     * @return ClassTemplate|null
     * @throws \Exception
     */
    protected function getStackTemplate(string $tag)
    {
        $template = $this->getTemplateStack()->getTemplateByTag($tag);
        if (!$template) {
            throw new \Exception('no '.$tag.'-template rendered yet');
        }

        return $template;
    }

    /**
     * getSettings
     *
     * @return BlueprintSettings
     */
    protected function getSettings()
    {
        return $this->settings;
    }

    /**
     * getPropertyInstructionsForTag
     *
     * @param string $tag
     *
     * @return PropertyInstructionContainer|null
     * @throws \Exception
     */
    protected function getPropertyInstructionsForTag(string $tag)
    {
        if (!$this->getSettings()) {
            throw new \Exception('Settings are not set!');
        }

        return $this->getSettings()->getPropertyInstructionsForTag($tag);
    }

    /**
     * setTemplateTagToClassTemplate
     *
     * @param string $tag
     *
     * @return AbstractBlueprint
     */
    protected function setTemplateTagToClassTemplate(string $tag)
    {
        $this->getClassTemplate()->setTemplateTag($tag);

        return $this;
    }

    /**
     * addSubDirectoryToPath
     *
     * @param string       $path
     * @param string|array $subDirectory
     *
     * @return string
     */
    protected function addSubDirectoryToPath(string $path, $subDirectory)
    {
        $subDirectories = [];
        if (is_string($subDirectory)) {
            $subDirectories[] = $subDirectory;
        } else {
            $subDirectories = $subDirectory;
        }

        return $path.DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $subDirectories);
    }

    /**
     * addToNamespace
     *
     * @param string $namespace
     * @param mixed  $parts
     *
     * @return string
     */
    public function addToNamespace(string $namespace, $parts)
    {
        if (is_string($parts)) {
            $parts = [$parts];
        }

        return $namespace.'\\'.implode('\\', $parts);
    }

    /**
     * getClassTemplate
     *
     * @return ClassTemplate
     */
    public function getClassTemplate()
    {
        return $this->getClassBuilder()->getClassTemplate();
    }

    /**
     * addStackTemplateAsProperty
     *
     * @param string $tag
     * @param bool   $createGetterAndSetter
     * @param bool   $nullable
     * @param bool   $assignInConstructor
     *
     * @return PropertyTemplate
     */
    public function addStackTemplateAsProperty(
        string $tag,
        bool $createGetterAndSetter = true,
        bool $nullable = false,
        $assignInConstructor = false
    ) {
        $template = $this->getStackTemplate($tag);

        return $this->getClassBuilder()->addPropertyByInstruction(
            PropertyInstruction::create(
                lcfirst($template->getClassBaseName()),
                $template->getFullClass(),
                $createGetterAndSetter,
                $nullable,
                $assignInConstructor
            )
        );
    }

    /**
     * createAnnotationInstructionByIdent
     *
     * @param string $ident
     * @param string $value
     *
     * @return BaseAnnotationInstruction
     */
    protected function createAnnotationInstructionByIdent(string $ident, string $value)
    {
        return BaseAnnotationInstruction::createByIdentAndValue($ident, $value);
    }
}
