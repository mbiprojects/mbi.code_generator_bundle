<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

/**
 * BlueprintStack
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
abstract class BlueprintStack
{

    /**
     * @var BlueprintStackItem[]
     */
    protected $items = [];

    /**
     * addItem
     *
     * @param string $blueprintClass
     * @param string $className
     *
     * @return BlueprintStack
     */
    public function addItem(string $blueprintClass, string $className)
    {
        $this->items[] = BlueprintStackItem::create($blueprintClass, $className);

        return $this;
    }

    /**
     * init
     *
     * @param string $entityName
     *
     * @return void
     */
    abstract public function init(string $entityName);

    /**
     * getItems
     *
     * @return BlueprintStackItem[]
     */
    public function getItems()
    {
        return $this->items;
    }
}
