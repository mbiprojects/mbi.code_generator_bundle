<?php

namespace Mbi\CodeGeneratorBundle\Library\Blueprint;

/**
 * BlueprintStackItem
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class BlueprintStackItem
{

    /**
     * @var string
     */
    protected $blueprintClass;

    /**
     * @var string
     */
    protected $className;

    /**
     * create
     *
     * @param string $blueprintClass
     * @param string $className
     *
     * @return BlueprintStackItem
     */
    public static function create(string $blueprintClass, string $className)
    {
        $item = new self();
        $item->setBlueprintClass($blueprintClass);
        $item->setClassName($className);

        return $item;
    }

    /**
     * getBlueprintClass
     *
     * @return string
     */
    public function getBlueprintClass()
    {
        return $this->blueprintClass;
    }

    /**
     * setBlueprintClass
     *
     * @param string $blueprintClass
     *
     * @return BlueprintStackItem
     */
    public function setBlueprintClass($blueprintClass)
    {
        $this->blueprintClass = $blueprintClass;

        return $this;
    }

    /**
     * getClassName
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * setClassName
     *
     * @param string $className
     *
     * @return BlueprintStackItem
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }
}
