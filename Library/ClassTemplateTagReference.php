<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassTemplateTagReference
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
final class ClassTemplateTagReference
{

    const ENTITY = 'entity';
    const ENTITY_FORM_DATA = 'form_data';
    const FACTORY = 'factory';
    const REPOSITORY = 'repository';
    const SERVICE = 'service';
}
