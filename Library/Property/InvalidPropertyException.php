<?php

namespace Mbi\CodeGeneratorBundle\Library\Property;

/**
 * InvalidPropertyException
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class InvalidPropertyException extends \Exception
{

}
