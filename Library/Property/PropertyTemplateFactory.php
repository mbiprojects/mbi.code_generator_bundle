<?php

namespace Mbi\CodeGeneratorBundle\Library\Property;

use Mbi\CodeGeneratorBundle\Library\DataType\DataType;

/**
 * PropertyFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class PropertyTemplateFactory
{

    /**
     * createTemplate
     *
     * @param string   $name
     * @param DataType $dataType
     * @param array    $options
     * @param bool     $assignInConstructor
     * @param bool     $isDoctrineProperty
     *
     * @return PropertyTemplate
     */
    public function createTemplate(
        string $name,
        DataType $dataType,
        array $options = [],
        bool $assignInConstructor = false,
        bool $isDoctrineProperty = false
    ) {
        $propertyTemplate = $this->createNewInstance();

        $propertyTemplate->setName($name);
        $propertyTemplate->setDataType($dataType);
        $propertyTemplate->setOptions($options);
        $propertyTemplate->setAssignInConstructor($assignInConstructor);
        $propertyTemplate->setIsDoctrineProperty($isDoctrineProperty);

        return $propertyTemplate;
    }

    /**
     * getClassTemplate
     *
     * @return PropertyTemplate
     */
    protected function createNewInstance()
    {
        return new PropertyTemplate();
    }
}
