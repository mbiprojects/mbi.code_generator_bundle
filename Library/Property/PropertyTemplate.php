<?php

namespace Mbi\CodeGeneratorBundle\Library\Property;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationSetAwareInterface;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationSetAwareTrait;
use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\Method\IsArgumentableTemplateInterface;

/**
 * PropertyTemplate
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class PropertyTemplate implements AnnotationSetAwareInterface, IsArgumentableTemplateInterface
{

    use AnnotationSetAwareTrait;

    const VISIBILITY_PROTECTED = 'protected';
    const VISIBILITY_PRIVATE = 'private';
    const VISIBILITY_PUBLIC = 'public';

    const OPTION_NULLABLE = 'nullable';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $visibility = self::VISIBILITY_PROTECTED;

    /**
     * @var DataType
     */
    protected $dataType;

    /**
     * @var bool
     */
    protected $assignInConstructor;

    /**
     * @var bool
     */
    protected $isDoctrineProperty = false;

    /**
     * @var array
     */
    protected $options;

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PropertyTemplate
     */
    public function setName(string $name): PropertyTemplate
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getVisibility
     *
     * @return string
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     *
     * @return PropertyTemplate
     */
    public function setVisibility(string $visibility): PropertyTemplate
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * getDataType
     *
     * @return DataType
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * getDatatTypeSignature
     *
     * @return string
     */
    public function getDataTypeSignature()
    {
        return $this->dataType->getTypeHint();
    }

    /**
     * @param DataType $dataType
     *
     * @return PropertyTemplate
     */
    public function setDataType(DataType $dataType): PropertyTemplate
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * getNullable
     *
     * @return bool
     */
    public function isNullable()
    {
        if (array_key_exists(self::OPTION_NULLABLE, $this->options)) {
            return $this->options[self::OPTION_NULLABLE];
        }

        return null;
    }

    /**
     * @param bool $nullable
     *
     * @return PropertyTemplate
     */
    public function setNullable(bool $nullable): PropertyTemplate
    {
        $this->options[self::OPTION_NULLABLE] = $nullable;

        return $this;
    }

    /**
     * getAssignInConstructor
     *
     * @return bool
     */
    public function getAssignInConstructor()
    {
        return $this->assignInConstructor;
    }

    /**
     * @param bool $assignInConstructor
     *
     * @return PropertyTemplate
     */
    public function setAssignInConstructor(bool $assignInConstructor): PropertyTemplate
    {
        $this->assignInConstructor = $assignInConstructor;

        return $this;
    }

    /**
     * isDoctrineProperty
     *
     * @return bool
     */
    public function isDoctrineProperty()
    {
        return $this->isDoctrineProperty;
    }

    /**
     * setIsDoctrineProperty
     *
     * @param bool $isDoctrineProperty
     *
     * @return PropertyTemplate
     */
    public function setIsDoctrineProperty($isDoctrineProperty)
    {
        $this->isDoctrineProperty = $isDoctrineProperty;

        return $this;
    }

    /**
     * setOptions
     *
     * @param array $options
     *
     * @return void
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * getOptions
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * hasOption
     *
     * @param string $optionName
     *
     * @return boolean
     */
    public function hasOption(string $optionName)
    {
        return array_key_exists($optionName, $this->options);
    }

    /**
     * getOption
     *
     * @param string $optionName
     *
     * @return mixed
     */
    public function getOption(string $optionName)
    {
        return array_key_exists($optionName, $this->options) ? $this->options[$optionName] : null;
    }
}
