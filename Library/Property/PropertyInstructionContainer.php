<?php

namespace Mbi\CodeGeneratorBundle\Library\Property;

/**
 * PropertyInstructionContainer
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class PropertyInstructionContainer
{

    /**
     * @var PropertyInstruction[]|array
     */
    protected $instructions = [];

    /**
     * @var string
     */
    protected $tag;

    /**
     * __construct
     *
     * @param string $useForTemplateWithTag
     */
    public function __construct(string $useForTemplateWithTag)
    {
        $this->tag = $useForTemplateWithTag;
    }

    /**
     * addInstruction
     *
     * @param PropertyInstruction $instruction
     *
     * @return PropertyInstructionContainer
     */
    public function addInstruction(PropertyInstruction $instruction)
    {
        $this->instructions[] = $instruction;

        return $this;
    }

    /**
     * getInstructions
     *
     * @return PropertyInstruction[]|array
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * getTag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }
}
