<?php

namespace Mbi\CodeGeneratorBundle\Library\Property;

use Mbi\CodeGeneratorBundle\Library\DataType\DataType;

/**
 * PropertyInstruction
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class PropertyInstruction
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|DataType
     */
    protected $propertyType;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var bool
     */
    protected $createGetterSetter;

    /**
     * @var bool
     */
    protected $isDoctrineProperty = false;

    /**
     * @var bool
     */
    protected $assignInConstructor = false;

    /**
     * create
     *
     * @param string $name
     * @param string $type
     * @param bool   $createGetterSetter
     *
     * @param bool   $nullable
     * @param bool   $assignInConstructor
     * @param bool   $isDoctrineProperty
     *
     * @return PropertyInstruction
     */
    public static function create(
        string $name,
        $type,
        bool $createGetterSetter = true,
        bool $nullable = false,
        bool $assignInConstructor = false,
        bool $isDoctrineProperty = false
    ) {
        $instruction = new self();

        $instruction->setName($name);
        $instruction->setPropertyType($type);
        $instruction->setCreateGetterSetter($createGetterSetter);
        $instruction->setNullable($nullable);
        $instruction->setAssignInConstructor($assignInConstructor);
        $instruction->setIsDoctrineProperty($isDoctrineProperty);

        return $instruction;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PropertyInstruction
     */
    public function setName(string $name): PropertyInstruction
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getPropertyType
     *
     * @return string
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * @param DataType|string $propertyType
     *
     * @return PropertyInstruction
     */
    public function setPropertyType($propertyType): PropertyInstruction
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * getCreateGetterSetter
     *
     * @return bool
     */
    public function getCreateGetterSetter()
    {
        return $this->createGetterSetter;
    }

    /**
     * @param bool $createGetterSetter
     *
     * @return PropertyInstruction
     */
    public function setCreateGetterSetter(bool $createGetterSetter): PropertyInstruction
    {
        $this->createGetterSetter = $createGetterSetter;

        return $this;
    }

    /**
     * isDoctrineProperty
     *
     * @return bool
     */
    public function isDoctrineProperty()
    {
        return $this->isDoctrineProperty;
    }

    /**
     * setIsDoctrineProperty
     *
     * @param bool $isDoctrineProperty
     *
     * @return PropertyInstruction
     */
    public function setIsDoctrineProperty($isDoctrineProperty)
    {
        $this->isDoctrineProperty = $isDoctrineProperty;

        return $this;
    }

    /**
     * getNullable
     *
     * @return bool
     */
    public function getNullable()
    {
        return (array_key_exists('nullable', $this->options) ? $this->options['nullable'] : false);
    }

    /**
     * setNullable
     *
     * @param bool $nullable
     *
     * @return PropertyInstruction
     */
    public function setNullable($nullable)
    {
        $this->options['nullable'] = $nullable;

        return $this;
    }

    /**
     * getAssignInConstructor
     *
     * @return bool
     */
    public function getAssignInConstructor()
    {
        return $this->assignInConstructor;
    }

    /**
     * setAssignInConstructor
     *
     * @param bool $assignInConstructor
     *
     * @return PropertyInstruction
     */
    public function setAssignInConstructor($assignInConstructor)
    {
        $this->assignInConstructor = $assignInConstructor;

        return $this;
    }

    /**
     * clone
     *
     * @return PropertyInstruction
     */
    public function clone()
    {
        return clone $this;
    }

    /**
     * addOption
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return PropertyInstruction
     */
    public function addOption(string $name, $value)
    {
        $this->options[$name] = $value;

        return $this;
    }

    /**
     * getOptions
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * setOptions
     *
     * @param array $options
     *
     * @return PropertyInstruction
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }
}
