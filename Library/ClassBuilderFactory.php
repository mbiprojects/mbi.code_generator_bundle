<?php

namespace Mbi\CodeGeneratorBundle\Library;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationDecorator;
use Mbi\CodeGeneratorBundle\Library\Render\ClassRenderer;

/**
 * ClassBuilderFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassBuilderFactory
{

    /**
     * @var ClassRenderer
     */
    private $classRenderer;

    /**
     * @var ClassProxyFactory
     */
    private $classProxyFactory;

    /**
     * @var ClassWriter
     */
    private $classWriter;

    /**
     * @var AnnotationDecorator
     */
    private $annotationDecorator;

    /**
     * @var TemplateFactoryProvider
     */
    private $templateFactoryProvider;

    /**
     * __construct
     *
     * @param TemplateFactoryProvider $templateFactoryProvider
     * @param AnnotationDecorator     $annotationDecorator
     * @param ClassProxyFactory       $classProxyFactory
     * @param ClassRenderer           $classRenderer
     * @param ClassWriter             $classWriter
     */
    public function __construct(
        TemplateFactoryProvider $templateFactoryProvider,
        AnnotationDecorator $annotationDecorator,
        ClassProxyFactory $classProxyFactory,
        ClassRenderer $classRenderer,
        ClassWriter $classWriter
    ) {
        $this->templateFactoryProvider = $templateFactoryProvider;
        $this->annotationDecorator     = $annotationDecorator;
        $this->classProxyFactory       = $classProxyFactory;
        $this->classRenderer           = $classRenderer;
        $this->classWriter             = $classWriter;
    }

    /**
     * createBuilder
     *
     * @return ClassBuilder
     */
    public function createBuilder()
    {
        return $this->createNewInstance();
    }

    /**
     * createNewInstance
     *
     * @return ClassBuilder
     */
    protected function createNewInstance()
    {
        return new ClassBuilder(
            $this->templateFactoryProvider->getClassTemplateFactory(),
            $this->templateFactoryProvider->getPropertyTemplateFactory(),
            $this->templateFactoryProvider->getMethodTemplateFactory(),
            $this->annotationDecorator,
            $this->templateFactoryProvider->getDataTypeFactory(),
            $this->classProxyFactory,
            $this->classRenderer,
            $this->classWriter
        );
    }
}
