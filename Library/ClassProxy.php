<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassProxy
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassProxy
{

    /**
     * @var string
     */
    protected $fullQualifiedClassName;

    /**
     * @var string
     */
    protected $classBaseName;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $path;

    /**
     * getFullQualifiedClassName
     *
     * @return string
     */
    public function getFullQualifiedClassName()
    {
        return $this->fullQualifiedClassName;
    }

    /**
     * @param string $fullQualifiedClassName
     *
     * @return ClassProxy
     */
    public function setFullQualifiedClassName(string $fullQualifiedClassName): ClassProxy
    {
        $this->fullQualifiedClassName = $fullQualifiedClassName;

        return $this;
    }

    /**
     * getClassBaseName
     *
     * @return string
     */
    public function getClassBaseName()
    {
        return $this->classBaseName;
    }

    /**
     * @param string $classBaseName
     *
     * @return ClassProxy
     */
    public function setClassBaseName(string $classBaseName): ClassProxy
    {
        $this->classBaseName = $classBaseName;

        return $this;
    }

    /**
     * getNamespace
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     *
     * @return ClassProxy
     */
    public function setNamespace(string $namespace): ClassProxy
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return ClassProxy
     */
    public function setPath(string $path): ClassProxy
    {
        $this->path = $path;

        return $this;
    }
}
