<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassProxyFactory
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassProxyFactory
{

    /**
     * createClassProxy
     *
     * @param string $fullQualifiedClassName
     *
     * @return ClassProxy
     */
    public function createClassProxy(string $fullQualifiedClassName)
    {
        $classProxy = $this->createNewInstance();

        $this->mapClassProxy($fullQualifiedClassName, $classProxy);

        return $classProxy;
    }

    /**
     * createByProperties
     *
     * @param string $classBaseName
     * @param string $namespace
     *
     * @param string $path
     *
     * @return ClassProxy
     */
    public function createByBaseNameAndNamespace(string $classBaseName, string $namespace, string $path)
    {
        $classProxy = $this->createNewInstance();

        $classProxy->setFullQualifiedClassName($namespace.'\\'.$classBaseName);
        $classProxy->setNamespace($namespace);
        $classProxy->setClassBaseName($classBaseName);
        $classProxy->setPath($path);

        return $classProxy;
    }

    /**
     * createNewInstance
     *
     * @return ClassProxy
     */
    protected function createNewInstance()
    {
        return new ClassProxy();
    }

    /**
     * mapClassProxy
     *
     * @param string     $fullQualifiedClassName
     * @param ClassProxy $classProxy
     *
     * @return ClassProxy
     * @throws \Exception
     */
    protected function mapClassProxy(string $fullQualifiedClassName, ClassProxy $classProxy)
    {
        if (!interface_exists($fullQualifiedClassName)
            && !(class_exists($fullQualifiedClassName))
            && !trait_exists($fullQualifiedClassName)
        ) {
            throw new \Exception($fullQualifiedClassName.' does not exist!');
        }

        $reflectedClass = new \ReflectionClass($fullQualifiedClassName);

        $namespace = $reflectedClass->getNamespaceName();
        $baseName  = $reflectedClass->getShortName();
        if (!$namespace) {
            $fullQualifiedClassName = stripcslashes('\\'.$fullQualifiedClassName);
            $baseName               = stripcslashes('\\'.$baseName);
        }

        $classProxy->setFullQualifiedClassName($fullQualifiedClassName);
        $classProxy->setNamespace($reflectedClass->getNamespaceName());
        $classProxy->setClassBaseName($baseName);
        $classProxy->setPath($reflectedClass->getFileName());

        return $classProxy;
    }

    /**
     * createParentClassProxy
     *
     * @param string $fqcn
     *
     * @return ClassProxy|null
     */
    public function createParentClassProxy(string $fqcn)
    {
        $reflectedClass = new \ReflectionClass($fqcn);

        if ($reflectedClass->getParentClass()) {
            return $this->createClassProxy($reflectedClass->getParentClass()->getName());
        }

        return null;
    }

    /**
     * getUsedInterfaceClassProxies
     *
     * @param string $fqcn
     *
     * @return ClassProxy[]|array
     */
    public function getUsedInterfaceClassProxies(string $fqcn)
    {
        $reflectedClass = new \ReflectionClass($fqcn);

        $interfaces = $reflectedClass->getInterfaces();

        $parentClassProxy = $this->createParentClassProxy($fqcn);
        if ($parentClassProxy) {
            $parentInterfaces = (new \ReflectionClass($parentClassProxy->getFullQualifiedClassName()))->getInterfaces();
            foreach ($parentInterfaces as $interfaceKey => $interface) {
                unset($interfaces[$interfaceKey]);
            }
        }

        $proxies = [];
        if (count($interfaces)) {
            foreach ($interfaces as $interface) {
                $proxy     = $this->createClassProxy($interface->getName());
                $proxies[] = $proxy;
            }
        }

        return $proxies;
    }

    /**
     * getUsedTraitClassProxies
     *
     * @param string $fqcn
     *
     * @return ClassProxy[]|array
     */
    public function getUsedTraitClassProxies(string $fqcn)
    {
        $reflectedClass = new \ReflectionClass($fqcn);

        $traits = $reflectedClass->getTraits();

        $proxies = [];
        if (count($traits)) {
            foreach ($traits as $trait) {
                $proxy     = $this->createClassProxy($trait->getName());
                $proxies[] = $proxy;
            }
        }

        return $proxies;
    }
}
