<?php

namespace Mbi\CodeGeneratorBundle\Library;

/**
 * ClassBuilderException
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassBuilderException extends \Exception
{

}
