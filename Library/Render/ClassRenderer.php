<?php

namespace Mbi\CodeGeneratorBundle\Library\Render;

use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * ClassRenderer
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassRenderer
{

    /**
     * @var string
     */
    protected $defaultClassTemplateFile = 'class.php.twig';

    /**
     * @var string
     */
    protected $defaultMethodTemplateFile = 'method.php.twig';

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var FilesystemLoader
     */
    private $filesystemLoader;

    /**
     * __construct
     *
     * @param FilesystemLoader $filesystemLoader
     * @param Environment      $twig
     */
    public function __construct(
        FilesystemLoader $filesystemLoader,
        Environment $twig
    ) {
        $this->twig             = $twig;
        $this->filesystemLoader = $filesystemLoader;
    }

    /**
     * renderClass
     *
     * @param ClassTemplate      $classTemplate
     *
     * @param array              $additionalData
     * @param ClassTemplateStack $templateStack
     *
     * @return string
     */
    public function renderClass(
        ClassTemplate $classTemplate,
        array $additionalData,
        ?ClassTemplateStack $templateStack = null
    ) {
        $templateData = [
            'classTemplate'      => $classTemplate,
            'templateStack'      => $templateStack,
            'methodTemplateFile' => $this->defaultMethodTemplateFile,
        ];
        $templateData = array_merge($templateData, $additionalData);

        return $this->renderTemplate(
            $this->getTemplatePath($classTemplate),
            $this->getTemplateFile($classTemplate),
            $templateData
        );
    }

    /**
     * renderTemplate
     *
     * @param string $path
     * @param string $templateFile
     * @param array  $params
     *
     * @return string
     */
    protected function renderTemplate(string $path, string $templateFile, array $params)
    {
        $this->filesystemLoader->addPath($path);

        return $this->twig->render($templateFile, $params);
    }

    /**
     * getTemplatePath
     *
     * @param ClassTemplate $classTemplate
     *
     * @return string
     */
    protected function getTemplatePath(ClassTemplate $classTemplate)
    {
        if ($classTemplate->getClassTemplatePath()) {
            return $classTemplate->getClassTemplatePath();
        }

        return self::getDefaultTemplatePath();
    }

    /**
     * getDefaultTemplatePath
     *
     * @return string
     */
    public static function getDefaultTemplatePath()
    {
        return __DIR__.DIRECTORY_SEPARATOR.'Template'.DIRECTORY_SEPARATOR;
    }

    /**
     * getTemplateFile
     *
     * @param ClassTemplate $classTemplate
     *
     * @return string
     */
    protected function getTemplateFile(ClassTemplate $classTemplate)
    {
        if ($classTemplate->getClassTemplateFile()) {
            return $classTemplate->getClassTemplateFile();
        }

        return $this->defaultClassTemplateFile;
    }
}
