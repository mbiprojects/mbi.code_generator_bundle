<?php

namespace Mbi\CodeGeneratorBundle\Library;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationDecorator;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Annotation\BaseAnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplate;
use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\DataType\DataTypeFactory;
use Mbi\CodeGeneratorBundle\Library\Method\ArgumentTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\IsArgumentableTemplateInterface;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplateFactory;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplate;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplateFactory;
use Mbi\CodeGeneratorBundle\Library\Render\ClassRenderer;

/**
 * ClassTemplateBuilder
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class ClassBuilder
{

    /**
     * @var ClassTemplateStack|null
     */
    protected $templateStack;

    /**
     * @var string
     */
    protected $renderedClass;

    /**
     * @var ClassTemplate
     */
    private $classTemplate;

    /**
     * @var PropertyTemplateFactory
     */
    private $propertyTemplateFactory;

    /**
     * @var MethodTemplateFactory
     */
    private $methodTemplateFactory;

    /**
     * @var AnnotationDecorator
     */
    private $annotationDecorator;

    /**
     * @var DataTypeFactory
     */
    private $dataTypeFactory;

    /**
     * @var ClassProxyFactory
     */
    private $classProxyFactory;

    /**
     * @var ClassRenderer
     */
    private $classRenderer;

    /**
     * @var ClassWriter
     */
    private $classWriter;

    /**
     * @var ClassTemplateFactory
     */
    private $classTemplateFactory;

    /**
     * __construct
     *
     * @param ClassTemplateFactory    $classTemplateFactory
     * @param PropertyTemplateFactory $propertyTemplateFactory
     * @param MethodTemplateFactory   $methodTemplateFactory
     * @param AnnotationDecorator     $annotationDecorator
     * @param DataTypeFactory         $dataTypeFactory
     * @param ClassProxyFactory       $classProxyFactory
     * @param ClassRenderer           $classRenderer
     * @param ClassWriter             $classWriter
     */
    public function __construct(
        ClassTemplateFactory $classTemplateFactory,
        PropertyTemplateFactory $propertyTemplateFactory,
        MethodTemplateFactory $methodTemplateFactory,
        AnnotationDecorator $annotationDecorator,
        DataTypeFactory $dataTypeFactory,
        ClassProxyFactory $classProxyFactory,
        ClassRenderer $classRenderer,
        ClassWriter $classWriter
    ) {
        $this->classTemplateFactory    = $classTemplateFactory;
        $this->propertyTemplateFactory = $propertyTemplateFactory;
        $this->methodTemplateFactory   = $methodTemplateFactory;
        $this->annotationDecorator     = $annotationDecorator;
        $this->dataTypeFactory         = $dataTypeFactory;
        $this->classProxyFactory       = $classProxyFactory;
        $this->classRenderer           = $classRenderer;
        $this->classWriter             = $classWriter;
    }

    /**
     * initClassTemplate
     *
     * @param string      $baseClassName
     * @param string      $path
     * @param string      $namespace
     * @param string|null $parentClass
     *
     * @return void
     */
    public function initClassTemplate(
        string $baseClassName,
        string $path,
        string $namespace,
        ?string $parentClass = null
    ) {
        $this->classTemplate = $this->classTemplateFactory->createTemplate(
            $baseClassName,
            $path,
            $namespace,
            $parentClass
        );
    }

    /**
     * addPropertyByInstruction
     *
     * @param PropertyInstruction     $propertyInstruction
     * @param AnnotationInstruction[] $annotationInstructions
     *
     * @return PropertyTemplate
     */
    public function addPropertyByInstruction(
        PropertyInstruction $propertyInstruction,
        array $annotationInstructions = []
    ) {
        //TODO: Refactor => remove addProperty-method and create template via instruction in factory
        $property = $this->addProperty(
            $propertyInstruction->getName(),
            $propertyInstruction->getPropertyType(),
            $propertyInstruction->getCreateGetterSetter(),
            $propertyInstruction->getOptions(),
            $propertyInstruction->getAssignInConstructor(),
            $propertyInstruction->isDoctrineProperty()
        );

        if (count($propertyInstruction->getOptions())) {
            $property->setOptions($propertyInstruction->getOptions());
        }

        if (count($annotationInstructions)) {
            $this->addPropertyAnnotations($property, $annotationInstructions);
        }

        return $property;
    }

    /**
     * addProperty
     *
     * @param string $name
     * @param string $type
     * @param bool   $createGetterAndSetter
     * @param array  $options
     * @param bool   $assignInConstructor
     * @param bool   $isDoctrineProperty
     *
     * @return PropertyTemplate
     */
    protected function addProperty(
        string $name,
        $type,
        bool $createGetterAndSetter = true,
        array $options = [],
        bool $assignInConstructor = false,
        bool $isDoctrineProperty = false
    ) {
        if (!($type instanceof DataType)) {
            $type = $this->dataTypeFactory->createByIdent($type);
        }

        $property = $this->propertyTemplateFactory->createTemplate(
            $name,
            $type,
            $options,
            $assignInConstructor,
            $isDoctrineProperty
        );

        $this->annotationDecorator->addAnnotationToTemplate(
            $property,
            BaseAnnotationInstruction::createByIdentAndValue(
                AnnotationTemplate::IDENT_VAR,
                $property->getDataTypeSignature()
            )
        );

        $this->getClassTemplate()->addProperty($property);

        if ($createGetterAndSetter) {
            $this->addGetter($property);
            $this->addSetter($property);
        }

        return $property;
    }

    /**
     * addGetter
     *
     * @param PropertyTemplate $propertyTemplate
     *
     * @return ClassBuilder
     */
    public function addGetter(PropertyTemplate $propertyTemplate)
    {
        $method = $this->methodTemplateFactory->createGetter($propertyTemplate);

        $this->addMethod($method);

        return $this;
    }

    /**
     * addSetter
     *
     * @param PropertyTemplate $propertyTemplate
     *
     * @return ClassBuilder
     */
    public function addSetter(PropertyTemplate $propertyTemplate)
    {
        $method = $this->methodTemplateFactory->createSetter($propertyTemplate);

        $this->addMethod($method);

        return $this;
    }

    /**
     * addMethod
     *
     * @param MethodTemplate $methodTemplate
     *
     * @return ClassBuilder
     */
    public function addMethod(MethodTemplate $methodTemplate)
    {
        $this->getClassTemplate()->addMethod($methodTemplate);

        return $this;
    }

    /**
     * addMethods
     *
     * @param array $methodTemplates
     *
     * @return ClassBuilder
     */
    public function addMethods(array $methodTemplates)
    {
        foreach ($methodTemplates as $methodTemplate) {
            $this->addMethod($methodTemplate);
        }

        return $this;
    }

    /**
     * getProperty
     *
     * @param string $name
     *
     * @return PropertyTemplate
     */
    public function getProperty(string $name)
    {
        return $this->getClassTemplate()->getProperty($name);
    }

    /**
     * getClassTemplate
     *
     * @return ClassTemplate
     * @throws \Exception
     */
    public function getClassTemplate()
    {
        if (is_null($this->classTemplate)) {
            throw new \Exception('class-template not initialized yet!');
        }

        return $this->classTemplate;
    }

    /**
     * useTrait
     *
     * @param string $trait
     *
     * @return ClassBuilder
     */
    public function useTrait(string $trait)
    {
        $this->getClassTemplate()->addTrait($this->classProxyFactory->createClassProxy($trait));

        return $this;
    }

    /**
     * addInterface
     *
     * @param string $interface
     *
     * @return ClassBuilder
     */
    public function implementInterface(string $interface)
    {
        $this->getClassTemplate()->addInterface($this->classProxyFactory->createClassProxy($interface));

        return $this;
    }

    /**
     * writeToFile
     *
     * @return ClassTemplate
     * @throws \Exception
     */
    public function writeToFile()
    {
        $fileName      = $this->getClassTemplate()->getClassBaseName().'.php';
        $fileDirectory = $this->getClassTemplate()->getPath();

        if (!$this->renderedClass) {
            throw new \Exception('class not rendered yet! Call render-method on classBuilder!');
        }

        $this->classWriter->writeToFile($fileDirectory, $fileName, $this->renderedClass);

        return $this->getClassTemplate();
    }

    /**
     * render
     *
     * @param array $additionalData
     *
     * @return ClassBuilder
     */
    public function render(array $additionalData = [])
    {
        $this->renderedClass = $this->classRenderer->renderClass(
            $this->getClassTemplate(),
            $additionalData,
            $this->templateStack
        );

        return $this;
    }

    /**
     * createMethodAnnotation
     *
     * @param MethodTemplate        $methodTemplate
     * @param AnnotationInstruction $instruction
     *
     * @return ClassBuilder
     */
    public function createMethodAnnotation(
        MethodTemplate $methodTemplate,
        AnnotationInstruction $instruction
    ) {
        $this->annotationDecorator->addAnnotationToTemplate($methodTemplate, $instruction);

        return $this;
    }

    /**
     * addArgumentFromPropertyToMethod
     *
     * @param IsArgumentableTemplateInterface $template
     *
     * @return Method\ArgumentTemplate
     */
    public function createArgumentFromTemplate(IsArgumentableTemplateInterface $template)
    {
        return $this->methodTemplateFactory->createArgument(
            $template->getName(),
            $template->getDataType(),
            $template->getOptions()
        );
    }

    /**
     * getConstructorArguments
     *
     * @return ArgumentTemplate[]|array
     */
    public function getConstructorArguments()
    {
        $classTemplate    = $this->getClassTemplate();
        $setInConstructor = [];
        foreach ($classTemplate->getProperties() as $property) {
            if ($property->getAssignInConstructor()) {
                $setInConstructor[] = $this->createArgumentFromTemplate($property);
            }
        }

        return $setInConstructor;
    }

    /**
     * addArgumentFromPropertyToMethod
     *
     * @param string $argumentName
     * @param string $dataTypeIdent
     * @param array  $options
     *
     * @return Method\ArgumentTemplate
     */
    public function createArgument(
        string $argumentName,
        string $dataTypeIdent,
        array $options = []
    ) {
        if (class_exists($dataTypeIdent)) {
            $dataType = $this->dataTypeFactory->createClassType($dataTypeIdent);
        } else {
            $dataType = $this->dataTypeFactory->createByIdent($dataTypeIdent);
        }

        return $this->methodTemplateFactory->createArgument($argumentName, $dataType, $options);
    }

    /**
     * decorateAnnotations
     *
     * @param AnnotationInstruction[] $additionalClassInstructions
     *
     * @return ClassBuilder
     */
    public function decorateAnnotations(array $additionalClassInstructions = [])
    {
        //add default method annotations
        foreach ($this->getClassTemplate()->getMethods() as $method) {
            $this->annotationDecorator->decorateSignitureAnnotations($method);
            $this->annotationDecorator->decorateGetterSetterAnnotations($method);
        }

        //set additional instructions
        if (count($additionalClassInstructions)) {
            foreach ($additionalClassInstructions as $instruction) {
                $this->annotationDecorator->addAnnotationToTemplate($this->getClassTemplate(), $instruction);
            }
        }

        //plugin based annotations
        $this->annotationDecorator->executeDecoratorPluginsOnClassTemplate($this->getClassTemplate());

        //order annotations
        $this->annotationDecorator->orderAnnotations($this->getClassTemplate());

        return $this;
    }

    /**
     * addClassAnnotation
     *
     * @param AnnotationInstruction $instruction
     *
     * @return ClassBuilder
     */
    public function addClassAnnotation(AnnotationInstruction $instruction)
    {
        $this->annotationDecorator->addAnnotationToTemplate($this->getClassTemplate(), $instruction);

        return $this;
    }

    /**
     * addPropertyAnnotations
     *
     * @param PropertyTemplate        $property
     * @param AnnotationInstruction[] $instructions
     *
     * @return ClassBuilder
     */
    public function addPropertyAnnotations(PropertyTemplate $property, array $instructions)
    {
        foreach ($instructions as $instruction) {
            $this->annotationDecorator->addAnnotationToTemplate($property, $instruction);
        }

        return $this;
    }

    /**
     * createMethod
     *
     * @param string      $name
     * @param array       $annotationInstructions
     * @param array       $arguments
     * @param \Closure    $contentCallback
     *
     * @param string|null $visibility
     *
     * @return MethodTemplate
     */
    public function createMethod(
        string $name,
        array $annotationInstructions = [],
        array $arguments = [],
        ?\Closure $contentCallback = null,
        ?string $visibility = null
    ) {
        $method = $this->methodTemplateFactory->createNewInstance()->setName($name);
        if (count($annotationInstructions)) {
            foreach ($annotationInstructions as $instruction) {
                if ($instruction instanceof AnnotationInstruction) {
                    $this->createMethodAnnotation($method, $instruction);
                }
            }
        }

        if (count($arguments)) {
            foreach ($arguments as $argument) {
                $method->addArgument($argument);
            }
        }

        if ($contentCallback) {
            $method->addContentCallback($contentCallback);
        }

        if ($visibility) {
            $method->setVisibility($visibility);
        }

        return $method;
    }
}
