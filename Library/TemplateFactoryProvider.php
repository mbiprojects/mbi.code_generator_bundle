<?php

namespace Mbi\CodeGeneratorBundle\Library;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationDecorator;
use Mbi\CodeGeneratorBundle\Library\DataType\DataTypeFactory;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplateFactory;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyTemplateFactory;

/**
 * TemplateFactoryProvider
 *
 * @author  Markus Bierau <markus.bierau@doccheck.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class TemplateFactoryProvider
{

    /**
     * @var PropertyTemplateFactory
     */
    private $propertyTemplateFactory;

    /**
     * @var MethodTemplateFactory
     */
    private $methodTemplateFactory;

    /**
     * @var DataTypeFactory
     */
    private $dataTypeFactory;

    /**
     * @var ClassTemplateFactory
     */
    private $classTemplateFactory;

    /**
     * __construct
     *
     * @param ClassTemplateFactory    $classTemplateFactory
     * @param PropertyTemplateFactory $propertyTemplateFactory
     * @param MethodTemplateFactory   $methodTemplateFactory
     * @param DataTypeFactory         $dataTypeFactory
     */
    public function __construct(
        ClassTemplateFactory $classTemplateFactory,
        PropertyTemplateFactory $propertyTemplateFactory,
        MethodTemplateFactory $methodTemplateFactory,
        DataTypeFactory $dataTypeFactory
    ) {
        $this->classTemplateFactory    = $classTemplateFactory;
        $this->propertyTemplateFactory = $propertyTemplateFactory;
        $this->methodTemplateFactory   = $methodTemplateFactory;
        $this->dataTypeFactory         = $dataTypeFactory;
    }

    /**
     * getClassTemplateFactory
     *
     * @return ClassTemplateFactory
     */
    public function getClassTemplateFactory()
    {
        return $this->classTemplateFactory;
    }

    /**
     * getPropertyTemplateFactory
     *
     * @return PropertyTemplateFactory
     */
    public function getPropertyTemplateFactory()
    {
        return $this->propertyTemplateFactory;
    }

    /**
     * getMethodTemplateFactory
     *
     * @return MethodTemplateFactory
     */
    public function getMethodTemplateFactory()
    {
        return $this->methodTemplateFactory;
    }

    /**
     * getAnnotationDecorator
     *
     * @return AnnotationDecorator
     */
    public function getAnnotationDecorator()
    {
        return $this->annotationDecorator;
    }

    /**
     * getDataTypeFactory
     *
     * @return DataTypeFactory
     */
    public function getDataTypeFactory()
    {
        return $this->dataTypeFactory;
    }
}
