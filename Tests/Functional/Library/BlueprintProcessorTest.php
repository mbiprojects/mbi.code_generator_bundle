<?php

namespace Mbi\CodeGeneratorBundle\Tests\Functional\Library;

use Mbi\CodeGeneratorBundle\Blueprint\Stack\EntityBlueprintStack;
use Mbi\CodeGeneratorBundle\Library\Blueprint\BlueprintProcessor;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;
use Mbi\CodeGeneratorBundle\Tests\MbiKernelTestCase;

/**
 * BlueprintProcessorTest
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class BlueprintProcessorTest extends MbiKernelTestCase
{

    /**
     * @var BlueprintProcessor|null
     */
    protected $blueprintProcessor;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->blueprintProcessor = self::$container->get('mbi_code_generator.blueprint_processor');

        parent::setUp();
    }

    /**
     * @inheritDoc
     */
    protected function tearDown()
    {
        //$this->clearTempPath();

        parent::tearDown();
    }

    /**
     * testCreateBlueprints
     *
     * @return void
     * @throws \Exception
     */
    public function testCreateBlueprints()
    {
        $entityName = 'Blog';
        $namespace  = 'Mbi\\CodeGeneratorBundle\\Tests\\Temp\\'.$entityName;
        $path       = $this->getTempPath();

        $propertyInstructions = $this->getPropertyInstructionsForEntity(
            [
                ['name' => 'title', 'type' => 'string', 'options' => ['nullable' => false, 'length' => 255]],
                ['name' => 'rank', 'type' => 'int', 'options' => ['nullable' => false]],
                ['name' => 'published', 'type' => 'bool', 'options' => ['nullable' => true]],
                ['name' => 'created', 'type' => 'datetime', 'options' => ['nullable' => true]],
            ]
        );

        $settings = $this->blueprintProcessor->createSettings();
        $settings->addPropertyInstructions($propertyInstructions);

        $templateStack = $this->blueprintProcessor->generateByStack(
            EntityBlueprintStack::class,
            $entityName,
            $path,
            $namespace,
            $settings
        );

        $this->assertInstanceOf(ClassTemplateStack::class, $templateStack);

        $classes = $templateStack->getGeneratedClasses();
        $this->assertCount(5, $classes);

        foreach ($classes as $class) {
            $this->assertTrue(class_exists($class));
        }
    }

    /**
     * getPropertyInstructions
     *
     * @param array $properties
     *
     * @return PropertyInstructionContainer
     */
    protected function getPropertyInstructionsForEntity(array $properties)
    {
        $propertyInstructions = new PropertyInstructionContainer(ClassTemplateTagReference::ENTITY);
        foreach ($properties as $index => $data) {
            $propertyInstructions->addInstruction(
                (new PropertyInstruction())->setName($data['name'])
                    ->setPropertyType($data['type'])
                    ->setCreateGetterSetter(true)
                    ->setOptions($data['options'])
            );
        }

        return $propertyInstructions;
    }
}
