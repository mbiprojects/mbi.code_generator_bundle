<?php

namespace Mbi\CodeGeneratorBundle\Tests;

use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * MbiKernelTestCase
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class MbiKernelTestCase extends KernelTestCase
{

    /**
     * getTempPath
     *
     * @return string
     */
    protected function getTempPath()
    {
        return dirname(__FILE__).DIRECTORY_SEPARATOR.'Temp';
    }

    /**
     * clearTempPath
     *
     * @return void
     */
    protected function clearTempPath()
    {
        $dir = $this->getTempPath();
        $di  = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
        $ri  = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($ri as $file) {
            $file->isDir() ? rmdir($file) : unlink($file);
        }
    }
}
