<?php

namespace Mbi\CodeGeneratorBundle\Tests\Unit\Command;

use Mbi\CodeGeneratorBundle\Blueprint\Stack\EntityBlueprintStack;
use Mbi\CodeGeneratorBundle\Command\GeneratePackageCommand;
use Mbi\CodeGeneratorBundle\Library\Blueprint\BlueprintProcessor;
use Mbi\CodeGeneratorBundle\Library\Blueprint\BlueprintSettings;
use Mbi\CodeGeneratorBundle\Library\ClassProxyFactory;
use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\DataType\DataTypeFactory;
use Mbi\CodeGeneratorBundle\Library\DataType\ObjectType;
use Mbi\CodeGeneratorBundle\Library\DataType\StringType;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateStack;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;
use Mbi\CodeGeneratorBundle\Tests\MbiKernelTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * BlueprintProcessorTest
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 DocCheck Medical Services GmbH
 */
class GeneratePackageCommandTest extends MbiKernelTestCase
{

    /**
     * @var BlueprintProcessor|MockObject
     */
    protected $blueprintProcessorMock;

    /**
     * @var GeneratePackageCommand
     */
    protected $command;

    /**
     * setUp
     *
     * @return
     */
    public function setUp()
    {
        parent::setUp();

        /** @var BlueprintProcessor|MockObject $blueprintProcessorMock */
        $this->blueprintProcessorMock = $this->getMockBuilder(BlueprintProcessor::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->command = new GeneratePackageCommand(
            $this->blueprintProcessorMock,
            new DataTypeFactory(new ClassProxyFactory()),
            __DIR__
        );
        $this->command->setApplication(new Application(static::createKernel()));
    }

    /**
     * @inheritDoc
     */
    public function tearDown()
    {
        $this->command                = null;
        $this->blueprintProcessorMock = null;
    }

    /**
     * testExecute
     *
     * @return void
     */
    public function testExecute()
    {
        $commandTester = new CommandTester($this->command);

        $commandTester->setInputs(
            [
                $entity = 'Blog', //Enter entity name
                $namespace = 'Mbi\Application\Content\Blog', //Enter namespace
                $relativePath = 'test', //set relative path
                'y', //add property
                'title', //first property
                '0', //first property type: 0 = string
                '0',  //nullable
                '255',  //length
                'y', //add property
                'created', //second property
                '3', //second property type: 3 = datetime
                '1', //nullable
                'n' //no further property
            ]
        );

        /** @var ClassTemplateStack|MockObject $templateStackMock */
        $templateStackMock = $this->getMockBuilder(ClassTemplateStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var ClassTemplate|MockObject $classTemplateMock */
        $classTemplateMock = $this->getMockBuilder(ClassTemplate::class)
            ->disableOriginalConstructor()
            ->getMock();
        $classTemplateMock->method('getFullClass')->willReturn('GeneratedClass');

        $templateStackMock->expects($this->once())->method('getGeneratedTemplates')->willReturn([$classTemplateMock]);

        $this->blueprintProcessorMock->expects($this->once())->method('createSettings')->willReturn(
            BlueprintSettings::create()
        );

        $this->blueprintProcessorMock->expects($this->once())->method('generateByStack')
            ->with(
                EntityBlueprintStack::class,
                $entity,
                __DIR__.DIRECTORY_SEPARATOR.$relativePath,
                $namespace,
                $this->callback(
                    function ($blueprintSettings) {
                        //expect settings
                        if (!($blueprintSettings instanceof BlueprintSettings)) {
                            throw new \Exception('expected instance of '.BlueprintSettings::class.' as parameter!');
                        }

                        //expect instructions
                        $instructionContainer = $blueprintSettings->getPropertyInstructionsForTag(
                            ClassTemplateTagReference::ENTITY
                        );
                        if (!($instructionContainer instanceof PropertyInstructionContainer
                            && count($instructionContainer->getInstructions()) == 2)
                        ) {
                            throw new \Exception('blueprintSettings have no propertyInstructionContainer!');
                        }

                        $instructions                  = $instructionContainer->getInstructions();
                        $instructionForTitleProperty   = $instructions[0];
                        $instructionForCreatedProperty = $instructions[1];
                        if (!($instructionForTitleProperty->getName() == 'title'
                            && $instructionForTitleProperty->getPropertyType() instanceof StringType
                            && $instructionForCreatedProperty->getName() == 'created'
                            && $instructionForCreatedProperty->getPropertyType() instanceof ObjectType
                        )) {
                            throw new \Exception('invalid property instructions!');
                        }

                        return true;
                    }
                )
            )
            ->willReturn($templateStackMock);

        //execute command
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();

        $this->assertContains('- GeneratedClass', $output);
    }
}
