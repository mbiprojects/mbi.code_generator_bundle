#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
# install deb packages
apt-get update \
    && apt-get install -y \
        unzip \
        git \
        wget \
        zlib1g-dev \
        libzip-dev \
        --no-install-recommends \
    && apt-get clean -y

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit
pecl install xdebug

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-configure zip --with-libzip=/usr/include
docker-php-ext-install pdo_mysql zip
docker-php-ext-enable xdebug
