<?php

namespace Mbi\CodeGeneratorBundle\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Annotation\BaseAnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplate;
use Mbi\CodeGeneratorBundle\Library\Blueprint\AbstractBlueprint;
use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;

/**
 * EntityFactoryBlueprint
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class EntityFactoryBlueprint extends AbstractBlueprint
{

    /**
     * @inheritDoc
     */
    public function build(
        string $className,
        string $path,
        string $namespace
    ) {
        $entityTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $path           = $this->addSubDirectoryToPath($path, $entityTemplate->getClassBaseName());

        //init class builder
        $this->initClassBuilder(
            $className,
            $path,
            $namespace
        );

        $this->setTemplateTagToClassTemplate(ClassTemplateTagReference::FACTORY);

        $templateStack = $this->getTemplateStack();
        if (!$templateStack) {
            return $this;
        }

        $entityTemplate  = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $entityClassName = $entityTemplate->getClassBaseName();
        $classTemplate   = $this->getClassTemplate();
        $classTemplate->addUsedClass($entityTemplate->getFullClass());

        //method createByData
        $this->getClassBuilder()->addMethod($this->buildCreateByDataMethod());

        //method createENTITY
        $createEntityMethod = $this->buildCreateEntityMethod(
            $entityTemplate,
            $entityClassName,
            $this->getPropertyInstructionsForTag(ClassTemplateTagReference::ENTITY)
        );
        $this->getClassBuilder()->addMethod($createEntityMethod);

        //method createInstance
        $createInstanceMethod = $this->buildCreateInstanceMethod($entityClassName);
        $this->getClassBuilder()->addMethod($createInstanceMethod);

        return $this;
    }

    /**
     * buildCreateEntityMethod
     *
     * @param ClassTemplate                $entityTemplate
     * @param string                       $entityClassName
     * @param PropertyInstructionContainer $propertyInstructions
     *F
     *
     * @return MethodTemplate
     */
    protected function buildCreateEntityMethod(
        ClassTemplate $entityTemplate,
        string $entityClassName,
        PropertyInstructionContainer $propertyInstructions
    ) {
        $method = $this->getClassBuilder()->createMethod(
            'create'.$entityClassName,
            [BaseAnnotationInstruction::createByIdentAndValue(AnnotationTemplate::IDENT_RETURN, $entityClassName)]
        );

        $setter = [];
        if ($propertyInstructions instanceof PropertyInstructionContainer) {
            foreach ($propertyInstructions->getInstructions() as $instruction) {
                $property = $entityTemplate->getProperty($instruction->getName());

                $setter[] = ['method' => 'set'.ucfirst($property->getName()), 'variable' => '$'.$property->getName()];
                $method->addArgument($this->getClassBuilder()->createArgumentFromTemplate($property));
            }
        }

        $method->addContentCallback(
            function () use ($setter) {
                $ws   = '        ';
                $code = $ws.'$entity = $this->createNewInstance();'."\n\n";

                foreach ($setter as $setterData) {
                    $code .= $ws.'$entity->'.$setterData['method'].'('.$setterData['variable'].');'."\n";
                }

                $code .= "\n".$ws.'return $entity;';

                return $code;
            }
        );

        return $method;
    }

    /**
     * buildCreateByDataMethod
     *
     * @return MethodTemplate
     */
    private function buildCreateByDataMethod()
    {
        $dataTemplate   = $this->getStackTemplate(ClassTemplateTagReference::ENTITY_FORM_DATA);
        $entityTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $entityDataName = lcfirst($dataTemplate->getClassBaseName());

        $method = $this->getClassBuilder()->createMethod(
            'createByData',
            [
                $this->createAnnotationInstructionByIdent(
                    AnnotationTemplate::IDENT_RETURN,
                    $entityTemplate->getClassBaseName()
                ),
            ],
            [$this->getClassBuilder()->createArgument($entityDataName, $dataTemplate->getFullClass())]
        );

        $propertyInstructions = $this->getPropertyInstructionsForTag(ClassTemplateTagReference::ENTITY);
        $method->addContentCallback(
            function () use ($propertyInstructions, $entityDataName) {
                $code = '';
                $code .= '$entity = $this->createNewInstance();'."\n";
                if ($propertyInstructions instanceof PropertyInstructionContainer) {
                    foreach ($propertyInstructions->getInstructions() as $instruction) {
                        $code .= '$entity->set'.ucfirst($instruction->getName()).'($'.$entityDataName.'->get'.
                            ucfirst($instruction->getName()).'());'."\n";
                    }
                }

                $code .= "\n".'return $entity;';

                return $code;
            }
        );

        return $method;
    }

    /**
     * buildCreateInstanceMethod
     *
     * @param string $entityClassName
     *
     * @return MethodTemplate
     */
    protected function buildCreateInstanceMethod(string $entityClassName)
    {
        return $this->getClassBuilder()->createMethod(
            'createNewInstance',
            [$this->createAnnotationInstructionByIdent(AnnotationTemplate::IDENT_RETURN, $entityClassName)],
            [],
            function () use ($entityClassName) {
                return 'return new '.$entityClassName.'();';
            },
            MethodTemplate::VISIBILITY_PROTECTED
        );
    }
}
