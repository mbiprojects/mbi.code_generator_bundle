<?php

namespace Mbi\CodeGeneratorBundle\Blueprint\Stack;

use Mbi\CodeGeneratorBundle\Blueprint\EntityDataBlueprint;
use Mbi\CodeGeneratorBundle\Blueprint\EntityFactoryBlueprint;
use Mbi\CodeGeneratorBundle\Blueprint\EntityRepositoryBlueprint;
use Mbi\CodeGeneratorBundle\Blueprint\EntityBlueprint;
use Mbi\CodeGeneratorBundle\Blueprint\EntityServiceBlueprint;
use Mbi\CodeGeneratorBundle\Library\Blueprint\BlueprintStack;

/**
 * EntityBlueprintStack
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class EntityBlueprintStack extends BlueprintStack
{

    const POSTFIX_FACTORY = 'Factory';
    const POSTFIX_REPOSITORY = 'Repository';
    const POSTFIX_SERVICE = 'Service';
    const POSTFIX_DATA_OBJECT = 'Data';

    /**
     * @inheritDoc
     */
    public function init(string $entityName)
    {
        //entity
        $this->addItem(EntityBlueprint::class, $entityName);

        //entity-data
        $this->addItem(
            EntityDataBlueprint::class,
            self::getSubpackageClassName($entityName, self::POSTFIX_DATA_OBJECT)
        );

        //factory
        $this->addItem(
            EntityFactoryBlueprint::class,
            self::getSubpackageClassName($entityName, self::POSTFIX_FACTORY)
        );

        //repository
        $this->addItem(
            EntityRepositoryBlueprint::class,
            self::getSubpackageClassName($entityName, self::POSTFIX_REPOSITORY)
        );

        //service
        $this->addItem(
            EntityServiceBlueprint::class,
            self::getSubpackageClassName($entityName, self::POSTFIX_SERVICE)
        );
    }

    /**
     * assembleSubpackageClassName
     *
     * @param string $entityName
     * @param string $subpackageClassPostFix
     *
     * @return string
     */
    public static function getSubpackageClassName(string $entityName, string $subpackageClassPostFix)
    {
        return $entityName.$subpackageClassPostFix;
    }
}
