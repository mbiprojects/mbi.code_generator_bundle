<?php

namespace Mbi\CodeGeneratorBundle\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Blueprint\AbstractBlueprint;
use Mbi\CodeGeneratorBundle\Library\ORM\RepositoryFunctionsTrait;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;
use Doctrine\ORM\EntityRepository;

/**
 * EntityRepositoryBlueprint
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class EntityRepositoryBlueprint extends AbstractBlueprint
{

    /**
     * @inheritDoc
     */
    public function build(
        string $className,
        string $path,
        string $namespace
    ) {
        $entityTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $path           = $this->addSubDirectoryToPath($path, $entityTemplate->getClassBaseName());

        //init class builder
        $this->initClassBuilder(
            $className,
            $path,
            $namespace,
            EntityRepository::class
        );

        $this->getClassBuilder()->useTrait(RepositoryFunctionsTrait::class);

        $this->setTemplateTagToClassTemplate(ClassTemplateTagReference::REPOSITORY);

        return $this;
    }
}
