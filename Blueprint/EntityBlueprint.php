<?php

namespace Mbi\CodeGeneratorBundle\Blueprint;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Mbi\CodeGeneratorBundle\Blueprint\Stack\EntityBlueprintStack;
use Mbi\CodeGeneratorBundle\Library\Blueprint\AbstractBlueprint;
use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\ORM\DoctrineAnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;

/**
 * EntityBlueprint
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class EntityBlueprint extends AbstractBlueprint
{

    private const ENTITY_SUBDIRECTORY = 'Entity';

    /**
     * @inheritDoc
     */
    public function build(
        string $className,
        string $path,
        string $namespace
    ) {
        //init class builder
        $this->initClassBuilder(
            $className,
            $this->addSubDirectoryToPath($path, [$className, self::ENTITY_SUBDIRECTORY]),
            $this->addToNamespace($namespace, self::ENTITY_SUBDIRECTORY)
        );

        $this->setTemplateTagToClassTemplate(ClassTemplateTagReference::ENTITY);

        //ad id
        $this->getClassBuilder()->addPropertyByInstruction(
            PropertyInstruction::create('id', DataType::INTEGER, true, false, false, true),
            [
                DoctrineAnnotationInstruction::createByClass(Id::class),
                DoctrineAnnotationInstruction::createByClass(GeneratedValue::class, ['strategy' => 'AUTO']),
            ]
        );

        //add properties from instructions
        $propertyInstructions = $this->getPropertyInstructionsForTag(ClassTemplateTagReference::ENTITY);
        if ($propertyInstructions instanceof PropertyInstructionContainer) {
            foreach ($propertyInstructions->getInstructions() as $instruction) {
                $instruction->setIsDoctrineProperty(true);
                $this->getClassBuilder()->addPropertyByInstruction($instruction);
            }
        }

        //add doctrine-entity annotation with factory class
        $this->getClassBuilder()->addClassAnnotation(
            DoctrineAnnotationInstruction::createByClass(
                Entity::class,
                [
                    'class' => $namespace.'\\'.
                        EntityBlueprintStack::getSubpackageClassName(
                            $className,
                            EntityBlueprintStack::POSTFIX_FACTORY
                        ),
                ]
            )
        );

        return $this;
    }
}
