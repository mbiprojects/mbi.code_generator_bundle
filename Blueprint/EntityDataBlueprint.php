<?php

namespace Mbi\CodeGeneratorBundle\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplate;
use Mbi\CodeGeneratorBundle\Library\Annotation\BaseAnnotationInstruction;
use Mbi\CodeGeneratorBundle\Library\Blueprint\AbstractBlueprint;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;

/**
 * EntityDataBlueprint
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class EntityDataBlueprint extends AbstractBlueprint
{

    /**
     * @inheritDoc
     */
    public function build(
        string $className,
        string $path,
        string $namespace
    ) {
        $entityTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $path           = $this->addSubDirectoryToPath($path, [$entityTemplate->getClassBaseName(), 'Form']);
        $namespace      = $this->addToNamespace($namespace, 'Form');

        //init class builder
        $this->initClassBuilder(
            $className,
            $path,
            $namespace
        );

        $this->setTemplateTagToClassTemplate(ClassTemplateTagReference::ENTITY_FORM_DATA);

        //set same properties like entity
        $propertyInstructions = $this->getPropertyInstructionsForTag(ClassTemplateTagReference::ENTITY);
        if ($propertyInstructions instanceof PropertyInstructionContainer) {
            foreach ($propertyInstructions->getInstructions() as $instruction) {
                $this->getClassBuilder()->addPropertyByInstruction(
                    $instruction->clone()->setIsDoctrineProperty(false)
                );
            }
        }

        //add init method
        $this->getClassBuilder()->addMethod($this->buildInitByEntityMethod());

        return $this;
    }

    /**
     * buildInitByEntityMethod
     *
     * @return MethodTemplate
     */
    private function buildInitByEntityMethod()
    {
        $entityTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $method         = $this->getClassBuilder()->createMethod(
            'initByEntity',
            [
                BaseAnnotationInstruction::createByIdentAndValue(
                    AnnotationTemplate::IDENT_RETURN,
                    $this->getClassTemplate()->getClassBaseName()
                ),
            ],
            [
                $this->getClassBuilder()->createArgument(
                    $entityTemplate->getClassVarName(),
                    $entityTemplate->getFullClass()
                ),
            ]
        );

        $propertyInstructions = $this->getPropertyInstructionsForTag(ClassTemplateTagReference::ENTITY);
        $entityName           = $entityTemplate->getClassVarName();
        $method->addContentCallback(
            function () use ($propertyInstructions, $entityName) {
                $code = '';
                if ($propertyInstructions instanceof PropertyInstructionContainer) {
                    foreach ($propertyInstructions->getInstructions() as $instruction) {
                        $code .= '$this->set'.ucfirst($instruction->getName()).'($'.$entityName.'->get'.
                            ucfirst($instruction->getName()).'());'."\n";
                    }
                }

                $code .= "\n".'return $this;';

                return $code;
            }
        );

        return $method;
    }
}
