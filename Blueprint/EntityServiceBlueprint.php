<?php

namespace Mbi\CodeGeneratorBundle\Blueprint;

use Mbi\CodeGeneratorBundle\Library\Annotation\AnnotationTemplate;
use Mbi\CodeGeneratorBundle\Library\Blueprint\AbstractBlueprint;
use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\Method\MethodTemplate;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;

/**
 * EntityServiceBlueprint
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class EntityServiceBlueprint extends AbstractBlueprint
{

    /**
     * @inheritDoc
     */
    public function build(
        string $className,
        string $path,
        string $namespace
    ) {
        $entityTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
        $path           = $this->addSubDirectoryToPath($path, $entityTemplate->getClassBaseName());

        //init class builder
        $this->initClassBuilder(
            $className,
            $path,
            $namespace
        );

        $this->setTemplateTagToClassTemplate(ClassTemplateTagReference::SERVICE);

        $this->getClassTemplate()->addUsedClass($entityTemplate->getFullClass());

        //init properties
        $this->addStackTemplateAsProperty(ClassTemplateTagReference::REPOSITORY, false, false, true);
        $this->addStackTemplateAsProperty(ClassTemplateTagReference::FACTORY, false, false, true);

        //init constructor
        $this->initConstructor();

        //create methods
        $this->getClassBuilder()->addMethods(
            [
                $this->buildStoreEntityByDataMethod(),
                $this->buildStoreEntityMethod(),
                $this->buildUpdateEntityMethod(),
                $this->buildDeleteEntityMethod(),
            ]
        );

        return $this;
    }

    /**
     * buildCreateEntityMethod
     *
     * @return MethodTemplate
     */
    protected function buildStoreEntityMethod()
    {
        $entityTemplate       = $this->getEntityTemplate();
        $entityClassName      = $entityTemplate->getClassBaseName();
        $propertyInstructions = $this->getSettings()->getPropertyInstructionsForTag(ClassTemplateTagReference::ENTITY);

        $method = $this->getClassBuilder()->createMethod(
            'store'.$entityClassName,
            [$this->createAnnotationInstructionByIdent(AnnotationTemplate::IDENT_RETURN, $entityClassName)]
        );

        $signatureParams = [];
        if ($propertyInstructions instanceof PropertyInstructionContainer) {
            foreach ($propertyInstructions->getInstructions() as $instruction) {
                $property          = $entityTemplate->getProperty($instruction->getName());
                $signatureParams[] = '$'.$property->getName();
                $method->addArgument($this->getClassBuilder()->createArgumentFromTemplate($property));
            }
        }

        $method->addContentCallback(
            function () use ($entityClassName, $signatureParams) {
                $ws   = '        ';
                $code = $ws.'$entity = $this->'.lcfirst($entityClassName).'Factory->create'
                    .$entityClassName.'('.implode(', ', $signatureParams).');'."\n";
                $code .= $ws.'$this->'.lcfirst($entityClassName).'Repository->persist($entity);'."\n\n";
                $code .= $ws.'return $entity;';

                return $code;
            }
        );

        return $method;
    }

    /**
     * buildStoreEntityByDataMethod
     *
     * @return MethodTemplate
     */
    protected function buildStoreEntityByDataMethod()
    {
        $entityClassName = $this->getEntityTemplate()->getClassBaseName();

        $method = $this->getClassBuilder()->createMethod(
            'store'.$entityClassName.'ByData',
            [$this->createAnnotationInstructionByIdent(AnnotationTemplate::IDENT_RETURN, $entityClassName)]
        );

        $dataTemplate = $this->getStackTemplate(ClassTemplateTagReference::ENTITY_FORM_DATA);
        $argumentName = lcfirst($dataTemplate->getClassBaseName());
        $argument     = $this->getClassBuilder()->createArgument($argumentName, $dataTemplate->getFullClass());
        $method->addArgument($argument);

        $entityVarName = lcfirst($entityClassName);

        $method->addContentCallback(
            function () use ($entityVarName, $argumentName) {
                $code = '$entity = $this->'.$entityVarName.'Factory->createByData($'.$argumentName.');'."\n";
                $code .= '$this->'.$entityVarName.'Repository->persist($entity);'."\n\n";
                $code .= 'return $entity;';

                return $code;
            }
        );

        return $method;
    }

    /**
     * buildUpdateEntityMethod
     *
     * @return MethodTemplate
     */
    protected function buildUpdateEntityMethod()
    {
        $entityTemplate  = $this->getEntityTemplate();
        $entityClassName = $entityTemplate->getClassBaseName();

        $entityVarName = lcfirst($entityClassName);

        return $this->getClassBuilder()->createMethod(
            'update'.$entityClassName,
            [$this->createAnnotationInstructionByIdent(AnnotationTemplate::IDENT_RETURN, '$this')],
            [$this->getClassBuilder()->createArgument($entityVarName, $entityTemplate->getFullClass())],
            function () use ($entityClassName, $entityVarName) {
                $ws   = '        ';
                $code = $ws.'$this->'.$entityVarName.'Repository->flush($'.$entityVarName.');'."\n\n";
                $code .= $ws.'return $this;';

                return $code;
            }
        );
    }

    /**
     * buildDeleteEntityMethod
     *
     * @return MethodTemplate
     */
    protected function buildDeleteEntityMethod()
    {
        $entityTemplate  = $this->getEntityTemplate();
        $entityClassName = $entityTemplate->getClassBaseName();
        $entityVarName   = lcfirst($entityClassName);

        return $this->getClassBuilder()->createMethod(
            'delete'.$entityClassName,
            [$this->createAnnotationInstructionByIdent(AnnotationTemplate::IDENT_RETURN, '$this')],
            [$this->getClassBuilder()->createArgument($entityVarName, $entityTemplate->getFullClass())],
            function () use ($entityClassName, $entityVarName) {
                $code = '$this->'.$entityVarName.'Repository->remove($'.$entityVarName.');'."\n\n";
                $code .= 'return $this;';

                return $code;
            }
        );
    }

    /**
     * getEntityTemplate
     *
     * @return ClassTemplate|null
     */
    protected function getEntityTemplate()
    {
        return $this->getStackTemplate(ClassTemplateTagReference::ENTITY);
    }
}
