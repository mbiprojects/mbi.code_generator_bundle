<?php

namespace Mbi\CodeGeneratorBundle\Command;

use Mbi\CodeGeneratorBundle\Blueprint\Stack\EntityBlueprintStack;
use Mbi\CodeGeneratorBundle\Library\Blueprint\BlueprintProcessor;
use Mbi\CodeGeneratorBundle\Library\ClassTemplate;
use Mbi\CodeGeneratorBundle\Library\DataType\DataType;
use Mbi\CodeGeneratorBundle\Library\DataType\DataTypeFactory;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstruction;
use Mbi\CodeGeneratorBundle\Library\Property\PropertyInstructionContainer;
use Mbi\CodeGeneratorBundle\Library\ClassTemplateTagReference;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * GeneratePackageCommands
 *
 * @author  Markus Bierau <markus.bierau@gmail.com>
 * @license 2020 MBI
 */
class GeneratePackageCommand extends Command
{

    const NAME = 'mbi:cgb:generate:package';

    protected static $defaultName = self::NAME;

    /**
     * @var BlueprintProcessor
     */
    private $blueprintProcessor;

    /**
     * @var string
     */
    private $projectDir;

    /**
     * @var DataTypeFactory
     */
    private $dataTypeFactory;

    /**
     * __construct
     *
     * @param BlueprintProcessor $blueprintProcessor
     * @param DataTypeFactory    $dataTypeFactory
     * @param string             $projectDir
     * @param string|null        $name
     */
    public function __construct(
        BlueprintProcessor $blueprintProcessor,
        DataTypeFactory $dataTypeFactory,
        string $projectDir,
        string $name = null
    ) {
        $this->blueprintProcessor = $blueprintProcessor;
        $this->dataTypeFactory    = $dataTypeFactory;
        $this->projectDir         = $projectDir;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setDescription(
            'This command generates a subpackge with entity (doctrine),'
            .' factory, repository and integrating service '
        );
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //request entity name
        $entityName = $this->requestUserInput('Enter name for Entity (like Blog): ', $input, $output);

        //request namespace
        $namespace = $this->requestUserInput(
            'Enter namespace for Subpackage (like Mbi\Application\Content\Blog): ',
            $input,
            $output
        );

        //request relative path
        $relativePath = $this->requestUserInput(
            'Enter relative path for Subpackage (will be appended: e.g. kernel-project-dir + src/Application/Content):',
            $input,
            $output
        );

        $path = $this->getFullPackagePath($relativePath);

        //request properties
        $propertyInstructions = $this->requestProperties($input, $output);
        $settings             = $this->blueprintProcessor->createSettings();
        $settings->addPropertyInstructions($propertyInstructions);

        //generate
        $templateStack = $this->blueprintProcessor->generateByStack(
            EntityBlueprintStack::class,
            $entityName,
            $path,
            $namespace,
            $settings
        );

        $output->writeln($templateStack->getCount().' classes generated');

        /** @var ClassTemplate $classTemplate */
        foreach ($templateStack->getGeneratedTemplates() as $classTemplate) {
            $output->writeln('- '.$classTemplate->getFullClass());
        }

        return 0;
    }

    /**
     * getFullPackagePath
     *
     * @param string $path
     *
     * @return string
     */
    public function getFullPackagePath(string $path)
    {
        return str_replace('../', '', $this->projectDir.DIRECTORY_SEPARATOR.$path);
    }

    /**
     * requestProperties
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return PropertyInstructionContainer
     */
    protected function requestProperties(InputInterface $input, OutputInterface $output)
    {
        $propertyContainer = new PropertyInstructionContainer(ClassTemplateTagReference::ENTITY);
        $continue          = true;
        while ($continue == true) {
            $continue = $this->confirmContinueInput('Create entity-property? (Y|n)', $input, $output);

            if ($continue) {
                $propertyName = $this->requestUserInput(
                    'Enter property-name: ',
                    $input,
                    $output
                );

                $allowedTypes      = implode(',', DataType::ALLOWED_SCALAR_TYPES);
                $propertyTypeIdent = $this->requestUserInputChoice(
                    'Enter property-type ('.$allowedTypes.'): ',
                    DataType::ALLOWED_SCALAR_TYPES,
                    'Type % is not valid!',
                    $input,
                    $output
                );

                $dataType    = $this->dataTypeFactory->createByIdent($propertyTypeIdent);
                $instruction = PropertyInstruction::create($propertyName, $dataType);
                foreach ($dataType->getOptions() as $option) {
                    $optionValue = $this->requestUserInput(
                        'type-option '.$option->getName().' ['.$option->getValidationRegex().']: ',
                        $input,
                        $output,
                        function ($answer) use ($option) {
                            preg_match($option->getValidationRegex(), $answer, $matches);

                            if (!count($matches)) {
                                throw new \RuntimeException(
                                    'Invalid option value: '.$option->getValidationRegex()
                                );
                            }

                            return $answer;
                        }
                    );

                    if ($optionValue) {
                        $instruction->addOption($option->getName(), $optionValue);
                    }
                }

                $propertyContainer->addInstruction($instruction);
            }
        }

        return $propertyContainer;
    }

    /**
     * requestUserInput
     *
     * @param string          $question
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param \Closure|null   $validatorCallback
     *
     * @return string
     */
    protected function requestUserInput(
        string $question,
        InputInterface $input,
        OutputInterface $output,
        ?\Closure $validatorCallback = null
    ) {
        $question = new Question($question);

        if ($validatorCallback) {
            $question->setValidator($validatorCallback);
            $question->setMaxAttempts(5);
        }

        return $this->getHelper('question')->ask($input, $output, $question);
    }

    /**
     * requestUserInput
     *
     * @param string          $question
     * @param array           $choices
     * @param string          $choiceErrorMsg
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return string
     */
    protected function requestUserInputChoice(
        string $question,
        array $choices,
        string $choiceErrorMsg,
        InputInterface $input,
        OutputInterface $output
    ) {
        return $this->getHelper('question')->ask(
            $input,
            $output,
            (new ChoiceQuestion($question, $choices))->setErrorMessage($choiceErrorMsg)
        );
    }

    /**
     * confirmContinueInput
     *
     * @param string          $question
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return boolean
     */
    protected function confirmContinueInput(
        string $question,
        InputInterface $input,
        OutputInterface $output
    ) {
        return $this->getHelper('question')->ask(
            $input,
            $output,
            new ConfirmationQuestion($question, true, '/^(y)/i')
        );
    }
}
