# CodeGeneratorBundle

A Symfony Bundle to create subpackages for your library. With entity (doctrine) + data-object, factory, repository and integrating service.
More blueprints and stacks may follow.

## Install
Require composer-package: 
```
composer require --dev mbi/code-generator-bundle:dev-master
```

Register bundle in your `bundles.php`: 
```
return [
    ...
    \Mbi\CodeGeneratorBundle\MbiCodeGeneratorBundle::class => ['dev' => true, 'test' => true],
    ...
]; 
```

Add to config (if you want): 
```
mbi_code_generator: 
    default_class_annotations: 
        author: "AUTHOR NAME"
        license: "LICENSE"
```

## Create subpackage with command
```
php bin/console --env=test mbi:cgb:generate:package
```
```
Enter name for Entity (like Blog): Blog
Enter namespace for Subpackage (like Mbi\Application\Content\Blog): Mbi\Application\Content\Blog
Enter relative path for Subpackage (will be appended: e.g. kernel-project-dir + src/Application/Content):src/Application/Content
Create entity-property? (Y|n)Y
Enter property-name: name
Enter property-type (string,bool,int,datetime,float): 
  [0] string
  [1] bool
  [2] int
  [3] datetime
  [4] float
 > 0
type-option nullable [/^(0|1)$/]: 0
type-option length [/^[0-9]+$/]: 255
Create entity-property? (Y|n)
Enter property-name: created
Enter property-type (string,bool,int,datetime,float): 
  [0] string
  [1] bool
  [2] int
  [3] datetime
  [4] float
 > 3
type-option nullable [/^(0|1)$/]: 0
Create entity-property? (Y|n)n
5 classes generated
- Mbi\Application\Content\Blog\Entity\Blog
- Mbi\Application\Content\Blog\Form\BlogData
- Mbi\Application\Content\Blog\BlogFactory
- Mbi\Application\Content\Blog\BlogRepository
- Mbi\Application\Content\Blog\BlogService

```

## Want more?
You may try to create and process your own blueprints, feel free:
Extend `Mbi\CodeGeneratorBundle\Library\Blueprint\AbstractBlueprint` and use class-builder to template a class:

```
$this->classBuilder = $this->classBuilderFactory->createBuilder();
$this->classBuilder->initClassTemplate(
    $baseClassName,
    $path,
    $namespace,
    $parentClass
);

$this->classBuilder->useTrait(TRAIT::class);
$this->classBuilder->implementInterface(INTERFACE::class);

$property = $this->getClassBuilder()->addPropertyByInstruction(
    PropertyInstruction::create('PROPERTY_NAME', DataType::INTEGER, true, false, false, true),
    [
        DoctrineAnnotationInstruction::createByClass(Id::class),
        DoctrineAnnotationInstruction::createByClass(GeneratedValue::class, ['strategy' => 'AUTO']),
    ]
);

$this->getClassBuilder()->createMethod(
    'METHOD_NAME',
    [BaseAnnotationInstruction::createByIdentAndValue(AnnotationTemplate::IDENT_RETURN,'entity')],
    [],
    function () use ($entityClassName) {
        return 'return new '.$entityClassName.'();';
    },
    MethodTemplate::VISIBILITY_PROTECTED
);
```
Have a look into the available blueprints (`Mbi\CodeGeneratorBundle\Blueprint`) to get a clue how it works and what is possible.

Use `Mbi\CodeGeneratorBundle\Library\Blueprint\BlueprintProcessor` to generate and persist class-template: 
```
$generatedTemplate = $this->get('mbi_code_generator.blueprint_processor')->generate(
    SomeBlueprint::class,
    'NewEntity',
    '/path/to/namespace',
    'Package/Namespace',
    BlueprintSettings::create(),
    TemplateStack::create()
);
```